/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;

import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.example.dtu.dtucourse31606_dspapp.R.id.container;

public class Filter_Realtime_Activity extends AppCompatActivity {
    AudioManager am;
    boolean isRecording = false;
    boolean continued = false;
    public static double[] a = {0};
    public static double[] b = {1, 0};
    public static int order = 1;
    public static boolean firstRun;
    public static SoundProcessor filterResponse;
    double[] xleft;
    double[] yleft;
    AudioRecord record;
    AudioTrack track;
    public static int fs;
    boolean newSubmit;

    TextView ioDelay;        // Global textView for posting text on-screen

    Switch simpleSwitch;
    SoundProcessor processed;
    static double[][] savedResponse;
    Spinner filterSpinner;        // Spinner that holds names of all files in DTU_DSP-folder
    Thread recAndPlay;

    boolean firstSoundEnabled;

    int count;

    public static int fmin;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    double maxVal;
    double minVal;

    public static float[][] zeros;
    public static float[][] poles;

    public static boolean isActive;

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true; // Sets a flag if this activity is active
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActive = false; // Lowers active flag if the activity is shut down
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_realtime);

        int permissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "Trebuie sa oferi acces la spatiul de stocare!", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 110);
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.RECORD_AUDIO}, 110);
            } else {
                Toast.makeText(this, "Descarc noi actualizari!", Toast.LENGTH_SHORT).show();
            }
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the four
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // initiate a Switch to check whether poles or zeros are currently placed
        simpleSwitch = (Switch) findViewById(R.id.pzswitch);

        // Creates an audio manager and gets a samplerate from the system - otherwise sample is set to 44100
        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        String sampleRateStr = am.getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE);
        fs = Integer.parseInt(sampleRateStr);
        if (fs == 0) {
            fs = 44100; // Use a default value if property not found
        }

        // Sets the sound volume to
        setVolumeControlStream(AudioManager.MODE_IN_COMMUNICATION);

        // Sets initial conditions for the activity to run
        newSubmit = false;
        firstRun = true;
        firstSoundEnabled = true;
        count = 0;
        fmin = 10;
        recAndPlay = new Thread();

        zeros = new float[2][2];
        zeros[0][0] = 0;
        zeros[0][1] = 0;

        poles = new float[2][2];
        poles[0][0] = 0;
        poles[0][1] = 0;

        a = new double[]{0};
        b = new double[]{1, 0};
        order = 1;


        initRecordAndTrack();

        getDeltaPulse();
    }

    @Override
    public void onBackPressed() {  // Shuts down the activity if the back button is pressed
        track.pause();
        isRecording = false;
        recAndPlay.interrupt();
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_userguide, menu);

        // This code creates a dropdown menu that contains the saved filters
        if (filtertab1_realtime.filterSpinner != null) {
            // Creates spinner element(dropdown meu) holding the names of all files in DTU_DSP/Filters-folder
            filterSpinner = filtertab1_realtime.filterSpinner;
            File folder = new File(Environment.getExternalStorageDirectory() + "/DTU_DSP/filters/");
            File[] listOfFiles = folder.listFiles(); //Creates a list of files from folder

            List<String> categories = new ArrayList<String>(); // Spinner Drop down elements
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    categories.add(listOfFiles[i].getName());
                }
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories); // Creating adapter for spinner
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Drop down layout style - list view with radio button
                filterSpinner.setAdapter(dataAdapter); // attaching data adapter to spinner
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            try {
                Intent i = new Intent(this, RealtimeInfoActivity.class);
                startActivity(i);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    filtertab1_realtime tab1 = new filtertab1_realtime();
                    return tab1;
                case 1:
                    filtertab2 tab2 = new filtertab2();
                    return tab2;
                case 2:
                    filtertab3 tab3 = new filtertab3();
                    return tab3;
                case 3:
                    filtertab4 tab4 = new filtertab4();
                    return tab4;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "COEFFICIENTS";
                case 1:
                    return "POLE/ZERO";
                case 2:
                    return "TRANSFER FUNCTION";
                case 3:
                    return "FREQUENCY ANALYSER";
                default:
                    return null;
            }
        }
    }


    // This case tree decides what to do, when specific on screen buttons are pressed
    public void buttonTapped(View view) {
        switch (view.getId()) {
            case R.id.BStart: // On start sound
                try {
                    if (firstSoundEnabled = true) { // if the button is press for the first time
                        startRecordAndPlay();    // Begins recording and playing microphone input
                    } else{
                        track.play(); // Play sound, as the microphone is already activated
                    }
                    am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                    if (filtertab3.filterResponsePlot != null) { // if filtertab3 is active
                        plotFilterResponse(); // plot the filter response of the current filter
                    }
                    Method m;
                    try { // show input/output sound latency
                        m = am.getClass().getMethod("getOutputLatency", int.class);
                        int latency = (Integer) m.invoke(am, AudioManager.STREAM_MUSIC);
                        ioDelay = (TextView) findViewById(R.id.ioLatency);
                        ioDelay.setText(String.valueOf(latency));
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.BStop:
                try {
                    stopRecordAndPlay(); // Stops the recording
                    continued = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.refresh: // refresh the filter coefficients
                try {
                    newSubmit = true;
                    firstRun = true;
                    if(filtertab2.poleZeroLayout != null){ //if filtertab2 is active, update the z-plane plot in accordance to the current filter coefficients
                        getCoef();
                        updatePlot(b, a, order);
                        filtertab2.plot.drawPZ(order, poles, zeros, filtertab2.zoomScale);
                        filtertab2.poleZeroLayout.invalidate();

                        filtertab2.poles = poles;
                        filtertab2.zeros = zeros;
                        filtertab2.order = order;

                        System.out.println("Poles: "+poles[0][0]/ filtertab2.w+" \u00B1 "+poles[0][1]/ filtertab2.h);
                        System.out.println("Zeros: "+zeros[0][0]/ filtertab2.w+" \u00B1 "+zeros[0][1]/ filtertab2.h);
                    }
                    plotFilterResponse();
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Submission failed - try again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                break;
            case R.id.loadFilter:
                try {
                    loadFilter(); // Load selected filter coefficients from the device storage
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.saveFilter:
                saveFilter(); // Save current filter coefficients to the device storage
                break;
        }
    }

    public void updatePlot(double[] b, double[] a, int order){ // Takes filter coefficients and transforms them to poles-zero coordinates
        double[] temp = a;
        a = new double[temp.length + 1];

        // Makes sure that a0 = 1
        for (int i = 0; i < temp.length + 1; i++) {
            if (i == 0) {
                a[0] = 1;
            } else {
                a[i] = temp[i - 1];
            }
        }

        // Calculates poles and zeros, using the calcRoots-function
        zeros = calcRoots(b, order);
        poles = calcRoots(a, order);

        // Scales the coordinates to the currents level of zoom on the z-plane
        for (int i = 0; i < order; i++){
            zeros[i][0] = zeros[i][0]* filtertab2.plot.radius;
            zeros[i][1] = zeros[i][1]* filtertab2.plot.radius;
            poles[i][0] = poles[i][0]* filtertab2.plot.radius;
            poles[i][1] = poles[i][1]* filtertab2.plot.radius;
        }

    }

    // Calculates roots of the characteristic equation, determined by the given filter coefficients
    // Can solve up to 2nd order equations
    private static float[][] calcRoots(double[] coef, int ord) {
        float[][] z = new float[ord][2];

        if (ord == 1) {                 // If the equation is a first order equation
            float a = (float) coef[0];
            float b = (float) coef[1];
            z[0][0] = -b / a;
            z[0][1] = 0.0f;
        }

        if (ord == 2) { // If the equation is a second order equation, solving a Quadratic equation
            float a = (float) coef[0];
            float b = (float) coef[1];
            float c = (float) coef[2];

            float d = (float) Math.pow(b, 2) - (4 * a * c);
            if (d < 0) {                                // if complex
                d = -d;
                z[0][0] = -b / (2 * a);
                z[1][0] = z[0][0];
                z[0][1] = (float) Math.sqrt(d) / (2 * a);
                z[1][1] = -z[0][1];

            } else if (d > 0) {                        //if purely real
                float temp = (float) Math.sqrt(d) / (2 * a);
                z[0][0] = -b / (2 * a) + temp;
                z[1][0] = -b / (2 * a) - temp;
                z[0][1] = 0.0f;
                z[1][1] = 0.0f;

            } else if (d == 0) {  //if d = 0 (also purely real)
                z[0][0] = -b / (2 * a);
                z[1][0] = -b / (2 * a);
                z[0][1] = 0.0f;
                z[1][1] = 0.0f;
            }
        }
        return z;
    }

    //
    private void initRecordAndTrack() {
        int minBufferSize = AudioRecord.getMinBufferSize(fs, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        record = new AudioRecord.Builder()
                .setAudioSource(MediaRecorder.AudioSource.DEFAULT)
                .setAudioFormat(new AudioFormat.Builder()
                        .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                        .setSampleRate(fs)
                        .setChannelMask(AudioFormat.CHANNEL_IN_MONO)
                        .build())
                .setBufferSizeInBytes(minBufferSize)
                .build();

        int maxJitter = AudioTrack.getMinBufferSize(fs, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
        track = new AudioTrack.Builder()
                .setAudioAttributes(new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_UNKNOWN)
                        .build())
                .setAudioFormat(new AudioFormat.Builder()
                        .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                        .setSampleRate(fs)
                        .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
                        .build())
                .setBufferSizeInBytes(maxJitter)
                .build();
    }

    private void startRecordAndPlay() {
        record.startRecording();
        recAndPlay = new Thread() {
            @Override
            public void run() {
                if (interrupted()) {
                    return;
                }
                try {
                    recordAndPlay();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        recAndPlay.start();
        track.play();
        isRecording = true;
    }

    private void stopRecordAndPlay() {
        track.pause();
    }


    private void recordAndPlay() throws IOException, InterruptedException {
        am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        String framesPerBuffer = am.getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER);
        int framesPerBufferInt = Integer.parseInt(framesPerBuffer);
        final byte[] lin = new byte[framesPerBufferInt];

        double[] storeDouble;

        xleft = new double[order];
        yleft = new double[order];

        firstSoundEnabled = false;

        maxVal = 0;
        minVal = 0;

        final int[] plotDynamicRange = {60};
        final boolean[] linLogstate = {false};

        int avg;

        double[] avgFiltered = new double[lin.length / 2];
        double[] avgUnfiltered = new double[lin.length / 2];

        while (true) {
            if (isRecording) {
                final int num = record.read(lin, 0, lin.length); // records track
                storeDouble = new double[num / 2];
                for (int i = 0; i < storeDouble.length; i++) {
                    storeDouble[i] = bit16ToDouble(lin[i * 2], lin[i * 2 + 1]);
                }
                final SoundProcessor sound = new SoundProcessor(storeDouble, fs);
                if (newSubmit == true) {
                    maxVal = 0;
                    minVal = 0;

                    //sound.continuesIIRfilter(a, b, order, newSubmit, xleft, yleft);
                    sound.IIRfilter(a, b, order);

                    getCoef();

                    xleft = new double[order];
                    yleft = new double[order];
                    for (int i = 0; i < order; i++) {
                        xleft[i] = sound.sound[sound.sound.length - order + i];
                        yleft[i] = sound.soundFiltered[sound.soundFiltered.length - order + i];
                    }
                    this.runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Filter coefficients updated", Toast.LENGTH_SHORT).show();
                        }
                    });

                    newSubmit = false;
                }
                processed = new SoundProcessor(storeDouble, fs);
                processed.IIRfilter(a, b, order);

                final byte[] play = doubleToByteArray(normalize(processed.soundFiltered));
                track.write(play, 0, play.length); // Plays track

                avg = getRefreshRate(filtertab4.RAspinner);

                for(int i = 0; i < sound.sound.length; i++){
                    avgFiltered[i] += processed.soundFiltered[i];
                    avgUnfiltered[i] += sound.sound[i];
                }

                for (int i = 0; i < order; i++) {
                    xleft[i] = processed.sound[processed.sound.length - order + i];
                    yleft[i] = processed.soundFiltered[processed.soundFiltered.length - order + i];
                }

                count++; // Determines the update speed of the spectrum analyser

                if ((filtertab4.dataArea != null) && (filtertab4.windowArea != null) && (count >= avg)) {
                    final int finalAvg = avg;
                    final double[] avgFil = avgFiltered;
                    final double[] avgUnfil = avgUnfiltered;

                    avgFiltered = new double[lin.length / 2];
                    avgUnfiltered = new double[lin.length / 2];

                    new Thread(new Runnable() {
                        public void run() {
                            if((filtertab4.linLogSpectrumSwitch.isChecked() != linLogstate[0]) || (Integer.parseInt(filtertab4.DRspinner.getSelectedItem().toString()) != plotDynamicRange[0])){
                                plotDynamicRange[0] = Integer.parseInt(filtertab4.DRspinner.getSelectedItem().toString());
                                linLogstate[0] = filtertab4.linLogSpectrumSwitch.isChecked();
                                filtertab4.window.drawMagnitudeSpectrumWindow(filtertab4.linLogSpectrumSwitch.isChecked(), plotDynamicRange[0]);
                                filtertab4.windowArea.postInvalidate();
                            }

                            if(filtertab4.unfilteredSwith.isChecked()){
                                for(int i = 0; i < avgFil.length; i++){
                                    avgFil[i] = avgFil[i]/ finalAvg;
                                }
                                final SoundProcessor filtered = new SoundProcessor(avgFil);
                                filtered.fft();
                                filtertab4.data.drawMagnitudeSpectrumData(filtered.soundTransformed, filtertab4.linLogSpectrumSwitch.isChecked(), plotDynamicRange[0]);
                            } else{
                                for(int i = 0; i < avgFil.length; i++){
                                    avgFil[i] /= finalAvg;
                                    avgUnfil[i] /= finalAvg;
                                }
                                final SoundProcessor filtered = new SoundProcessor(avgFil);
                                final SoundProcessor unfiltered = new SoundProcessor(avgUnfil);
                                filtered.fft();
                                unfiltered.fft();
                                filtertab4.data.drawMagnitudeSpectrumData(filtered.soundTransformed, unfiltered.soundTransformed, filtertab4.linLogSpectrumSwitch.isChecked(), plotDynamicRange[0]);
                            }
                            filtertab4.dataArea.postInvalidate();
                        }
                    }).start();

                    count = 0;
                }
            }
        }
    }

    private int getRefreshRate(Spinner spinner){
        int output;

        if (spinner != null){
            String selection = spinner.getSelectedItem().toString();

            if (selection == "Too Fast"){
                output = 2;
            } else if (selection == "Very Fast"){
                output = 3;
            } else if (selection == "Fast"){
                output = 5;
            }else if (selection == "Moderate"){
                output = 10;
            }else if (selection == "Slow"){
                output = 20;
            }else if (selection == "Very Slow"){
                output = 40;
            }else if (selection == "Extremely Slow"){
                output = 200;
            } else{
                output = 10;
            }
        }else{
            output = 10;
        }

        return output;
    }

    public double[] normalize(double[] sound){    // Normalizes sound to max value of a 16bit number (32767)
        if(sound != null) {
            for (int i = 0; i < sound.length; i++) {
                if (i == 0 && minVal == 0) {
                    minVal = sound[i];
                }
                if (sound[i] < minVal) {   // if new value is greater than previous peak value
                    minVal = sound[i];     // New value is the peak value
                }
                if (sound[i] > maxVal) {   // if new value is greater than previous peak value
                    maxVal = sound[i];     // New value is the peak value
                }
            }
            for (int i = 0; i < sound.length; i++) {
                sound[i] = (sound[i]) / (maxVal - minVal) * 30000; //32767
            }
        }
        return sound;
    }


    private double bit16ToDouble(byte two, byte one) {
        int temp;
        temp = (int) one;
        temp = temp << 8;
        temp = (temp & 0xFFFFFF00) | ((int) two & 0xFF);
        return (double) temp;
    }

    private byte[] doubleToByteArray(double[] data) {
        byte[] b = new byte[data.length * 2];            // Needed since doubles have a 16bit depth
        for (int i = 0; i < data.length; i++) {        // while bytes have a 8bit depth
            int temp = (int) data[i];
            b[i * 2] = (byte) (temp & 0xFF);
            b[i * 2 + 1] = (byte) ((temp >> 8) & 0xFF);
        }
        return b;
    }


    private void getCoef() throws IOException {
        final TextView bval = (TextView) findViewById(R.id.B);
        final TextView aval = (TextView) findViewById(R.id.A);
        final TextView ord = (TextView) findViewById(R.id.filterOrder);
        DecimalFormat decOut = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
        // Define filter order from user input
        String orderInput = ((EditText) findViewById(R.id.filterOrder)).getText().toString();  // Get string from users input
        if (!orderInput.equals("") && !orderInput.equals("0")) {                 // if string is not empty
            order = Integer.parseInt(orderInput);   // Parse the string to an int named "order"
        } else {                                      // if empty input
            order = 1;
            this.runOnUiThread(new Runnable() {
                public void run() {
                    ord.setText(String.valueOf(order));
                    Toast.makeText(getApplicationContext(), "Filter order was out of scope and has been corrected", Toast.LENGTH_SHORT).show(); // Print message on screen
                }
            });
        }

        // Define a-values from user input
        String aInput = ((EditText) findViewById(R.id.A)).getText().toString();
        if (!aInput.equals("") && !aInput.isEmpty()) { // If not empty input
            String aText = "";
            double[] temp = stringToDoubleArr(aInput);
            if (a.length < order) {
                a = new double[order];
                for (int i = 0; i < temp.length; i++) {
                    a[i] = temp[i];
                }
                for (int i = temp.length; i < order; i++) {
                    a[i] = 0;
                }
                for (int i = 0; i < a.length - 1; i++) {
                    aText = aText + String.valueOf(decOut.format(a[i])) + ",";
                }
                aText = aText + String.valueOf(decOut.format(a[a.length - 1]));
                final String finalAText = aText;
                this.runOnUiThread(new Runnable() {
                    public void run() {
                        aval.setText(finalAText);
                    }
                });

            } else if (a.length > order) {
                a = new double[order];
                for (int i = 0; i < order; i++) {
                    a[i] = temp[i];
                }
                for (int i = 0; i < a.length - 1; i++) {
                    aText = aText + String.valueOf(decOut.format(a[i])) + ",";
                }
                aText = aText + String.valueOf(decOut.format(a[a.length - 1]));
                final String finalAText = aText;
                this.runOnUiThread(new Runnable() {
                    public void run() {
                        aval.setText(finalAText);
                    }
                });
            } else if (a.length == order) {
                a = stringToDoubleArr(aInput);
                for (int i = 0; i < a.length - 1; i++) {
                    aText = aText + String.valueOf(decOut.format(a[i])) + ",";
                }
                aText = aText + String.valueOf(decOut.format(a[a.length - 1]));
                final String finalAText = aText;
                this.runOnUiThread(new Runnable() {
                    public void run() {
                        aval.setText(finalAText);
                    }
                });
            }
        } else {                      // If empty:
            a = new double[1];      // create an array of length 1
            a[0] = 0;               // Fill the spot with a zero
        }

        //Define b-values from user input
        String bInput = ((EditText) findViewById(R.id.B)).getText().toString();
        if (!bInput.equals("") && !bInput.isEmpty()) { // If not empty input
            String bText = "";
            double[] temp = stringToDoubleArr(bInput);
            if (b.length < order + 1) {
                b = new double[order + 1];
                for (int i = 0; i < temp.length; i++) {
                    b[i] = temp[i];
                }
                for (int i = temp.length; i < order + 1; i++) {
                    b[i] = 0;
                }
                for (int i = 0; i < b.length - 1; i++) {
                    bText = bText + String.valueOf(decOut.format(b[i])) + ",";
                }
                bText = bText + String.valueOf(decOut.format(b[b.length - 1]));
                final String finalBText = bText;
                this.runOnUiThread(new Runnable() {
                    public void run() {
                        bval.setText(finalBText);
                    }
                });
            } else if (b.length > order + 1) {
                b = new double[order + 1];
                for (int i = 0; i < order + 1; i++) {
                    b[i] = temp[i];
                }
                for (int i = 0; i < b.length - 1; i++) {
                    bText = bText + String.valueOf(decOut.format(b[i])) + ",";
                }
                bText = bText + String.valueOf(decOut.format(b[b.length - 1]));
                final String finalBText = bText;
                this.runOnUiThread(new Runnable() {
                    public void run() {
                        bval.setText(finalBText);
                    }
                });
            } else if (b.length == order + 1) {
                b = stringToDoubleArr(bInput);
                for (int i = 0; i < b.length - 1; i++) {
                    bText = bText + String.valueOf(decOut.format(b[i])) + ",";
                }
                bText = bText + String.valueOf(decOut.format(b[b.length - 1]));
                final String finalBText = bText;
                this.runOnUiThread(new Runnable() {
                    public void run() {
                        bval.setText(finalBText);
                    }
                });
            }
        } else {                                       // If empty
            b = new double[2];
            b[0] = 1;
            b[1] = 0;
        }
    }

    // Extracts coefficients from a string, seperates at commas
    private static double[] stringToDoubleArr(String string) {
        String[] strArray = string.split(",");
        double[] dArray = new double[strArray.length];
        for (int i = 0; i < strArray.length; i++) {
            dArray[i] = Double.parseDouble(strArray[i]);
        }
        return dArray;
    }

    private void getDeltaPulse() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                //This function feeds the filter a delta-pulse to obtain a clean filter-response
                InputStream fin = getResources().openRawResource(R.raw.deltapulse); // Streams data from deltapulse.wav file in the raw-folder
                DataInputStream dis = new DataInputStream(fin);                     // importing data from file
                byte[] wavInfo = new byte[256];                                     // Read a small part of the file (header)
                try {
                    dis.read(wavInfo, 0, wavInfo.length);                               // Writes data from file info wavInfo array
                } catch (IOException e) {
                    e.printStackTrace();
                }
                WaveHeader header = new WaveHeader(wavInfo);                        // gets info from bytes in array
                byte[] music = new byte[header.waveLength + header.headerLength];   // make array, length of file
                try {
                    dis.read(music, 0, music.length);                                   // Writes data into array
                } catch (IOException e) {
                    e.printStackTrace();
                }
                filterResponse = new SoundProcessor(music, header.headerLength, header.waveLength, header.sampleRate, 16); //Passes the data to
                try {
                    dis.close(); //close data stream
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public static void plotFilterResponse() throws IOException, InterruptedException {
        if (filtertab3.filterResponsePlot != null) {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    Switch linLogSwitch = filtertab3.linLogSwitch;
                    Boolean switchState = linLogSwitch.isChecked();
                    if (firstRun == true) {
                        //Does the actual filtering
                        filterResponse.IIRfilter(a, b, order);
                        filterResponse.fftOnFilteredSound();
                        filtertab3.filterResponsePlot.drawBodePlotData(filterResponse.complexFourier, switchState, false);
                        savedResponse = filterResponse.complexFourier;
                        firstRun = false;
                    } else {
                        filtertab3.filterResponsePlot.drawBodePlotData(savedResponse, switchState, false);
                    }
                }
            };
            thread.start();

            filtertab3.magnitudeLayout.postInvalidate();
            filtertab3.phaseLayout.postInvalidate();
        }
    }

    private void loadFilter() throws IOException {
        FileInputStream is;
        BufferedReader reader;

        final TextView order = (TextView) findViewById(R.id.filterOrder);
        final TextView bval = (TextView) findViewById(R.id.B);
        final TextView aval = (TextView) findViewById(R.id.A);

        if (filtertab1_realtime.filterSpinner != null) {
            // Creates spinner element(dropdown meu) holding the names of all files in DTU_DSP-folder
            filterSpinner = filtertab1_realtime.filterSpinner;
        }

        String filter = filterSpinner.getSelectedItem().toString();

        File file = new File(Environment.getExternalStorageDirectory() + "/DTU_DSP/filters/" + filter);
        if (file.exists()) { // if the file already exists delete it and create new
            is = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(is));
            String[] filterInfo = new String[3];

            for (int i = 0; i < 3; i++) {
                filterInfo[i] = reader.readLine();
            }
            order.setText(filterInfo[0]);
            bval.setText(filterInfo[1]);
            aval.setText(filterInfo[2]);
        } else {
            Toast.makeText(getApplicationContext(), "The filter is corrupted or do not exist", Toast.LENGTH_LONG).show();
        }

    }

    private void saveFilter() {
        String order = ((EditText) findViewById(R.id.filterOrder)).getText().toString();
        String bval = ((EditText) findViewById(R.id.B)).getText().toString();
        String aval = ((EditText) findViewById(R.id.A)).getText().toString();
        String filename = ((EditText) findViewById(R.id.nameFilter)).getText().toString();

        try {
            File file = new File(Environment.getExternalStorageDirectory() + "/DTU_DSP/filters/" + filename);

            FileWriter writer = new FileWriter(file);
            writer.append(order);
            writer.write(System.getProperty("line.separator"));
            writer.append(bval);
            writer.write(System.getProperty("line.separator"));
            writer.append(aval);
            writer.flush();
            writer.close();
            Toast.makeText(getApplicationContext(), "The filter \"" + filename + "\" has been saved", Toast.LENGTH_SHORT).show();

            if (filtertab1_realtime.filterSpinner != null) {
                // Creates spinner element(dropdown meu) holding the names of all files in DTU_DSP-folder
                filterSpinner = filtertab1_realtime.filterSpinner;
                File folder = new File(Environment.getExternalStorageDirectory() + "/DTU_DSP/filters/");
                File[] listOfFiles = folder.listFiles(); //Creates a list of files from folder

                List<String> categories = new ArrayList<String>(); // Spinner Drop down elements
                for (int i = 0; i < listOfFiles.length; i++) {
                    if (listOfFiles[i].isFile()) {
                        categories.add(listOfFiles[i].getName());
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories); // Creating adapter for spinner
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Drop down layout style - list view with radio button
                    filterSpinner.setAdapter(dataAdapter); // attaching data adapter to spinner
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Failed to save filter", Toast.LENGTH_SHORT).show();
        }
    }


}

