/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Nikolaj on 11-06-2017.
 */

public class PoleZeroPlot {

    ImageView layout;                 // Graph-window in use for plots
    Bitmap bmp;
    int w;
    int h;
    Canvas canvas;
    int[] centrum;

    float[][] poles;
    float[][] zeros;

    int radius;

    int zoomScale;
    int smallestDimension;

    // Takes in the PlotFrequencyResponse data, sample rate, PlotFrequencyResponse-window, filename and color-number based on presses
    public void initPoleZeroPlot(ImageView plotWindow, int width, int height, int smallSize) {
        layout = plotWindow;
        smallestDimension = smallSize;
        w = width;
        h = height;

        zoomScale = 3;

        radius = smallestDimension/zoomScale;

        bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

        centrum = new int[]{bmp.getWidth()/2, bmp.getHeight()/2};

        canvas = new Canvas(bmp);

        layout.setImageBitmap(bmp);

        zeros = new float[2][2];
        zeros[0][0] = 0;
        zeros[0][1] = 0;

        poles = new float[2][2];
        poles[0][0] = 0;
        poles[0][1] = 0;
    }

    public void drawCoordinatesSystem(){
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        // GRID LINES
        Paint grid = new Paint();
        grid.setColor(Color.LTGRAY);
        grid.setStyle(Paint.Style.STROKE);
        grid.setStrokeWidth(1);

        int gridSpace = radius/2;

        for (int i = h/2; i < h; i += gridSpace){
            canvas.drawLine(0,i,w,i,grid);
        }

        for (int i = h/2; i > 0; i -= gridSpace){
            canvas.drawLine(0,i,w,i,grid);
        }

        for (int i = w/2; i < w; i += gridSpace){
            canvas.drawLine(i,0,i,h,grid);
        }
        for (int i = w/2; i > 0; i -= gridSpace){
            canvas.drawLine(i,0,i,h,grid);
        }


        //SETUP OF OUTER EDGE
        Paint edgeColor = new Paint();
        edgeColor.setColor(Color.BLACK);
        edgeColor.setStyle(Paint.Style.STROKE);
        edgeColor.setStrokeWidth(10);

        canvas.drawLine(0,0,0,h,edgeColor);
        canvas.drawLine(0,0,w,0,edgeColor);
        canvas.drawLine(w,0,w,h,edgeColor);
        canvas.drawLine(0,h,w,h,edgeColor);

        // DRAWING UNIT CIRCLE AND MAIN AXIS
        Paint unitCircle = new Paint();
        unitCircle.setColor(Color.DKGRAY);
        unitCircle.setStyle(Paint.Style.STROKE);
        unitCircle.setStrokeWidth(3);
        canvas.drawCircle(w/2, h/2, radius, unitCircle);

        unitCircle.setStyle(Paint.Style.FILL);
        unitCircle.setStrokeWidth(2);
        canvas.drawLine(w/2,0,w/2,h,unitCircle);
        canvas.drawLine(0,h/2,w,h/2,unitCircle);

        unitCircle.setColor(Color.BLACK);
        unitCircle.setTextSize(30);
        unitCircle.setStyle(Paint.Style.FILL);
        canvas.drawText("Im", w/2+w*0.01f, h/2-(radius+radius*0.1f), unitCircle);
        canvas.drawText("Re", w/2+radius+radius*0.1f, h/2-h*0.01f, unitCircle);

    }

    public void drawPZ(int order, float[][] polesInput, float[][] zerosInput, int zoom){
        int oldRadius =radius;
        zoomScale = zoom;
        radius = smallestDimension/zoomScale;
        drawCoordinatesSystem();
        Paint paint = new Paint();
        paint.setColor(Color.parseColor("#990000")); // DTU red
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        paint.setTextSize(30);

        for (int i = 0; i < order; i++){
            zerosInput[i][0] = zerosInput[i][0]*radius/oldRadius;
            zerosInput[i][1] = zerosInput[i][1]*radius/oldRadius;
            polesInput[i][0] = polesInput[i][0]*radius/oldRadius;
            polesInput[i][1] = polesInput[i][1]*radius/oldRadius;
        }

        if(polesInput != poles) {
            poles = new float[order][2];
            //applies coordinates system centralization of the supplied coordinates
            for (int i = 0; i < order; i++) {
                for (int j = 0; j < 2; j++) {
                    poles[i][j] = polesInput[i][j]+centrum[j];
                }
            }
        }

        if(zerosInput != zeros) {
            zeros = new float[order][2];
            //applies coordinates system centralization of the supplied coordinates
            for (int i = 0; i < order; i++) {
                for (int j = 0; j < 2; j++) {
                    zeros[i][j] = zerosInput[i][j]+centrum[j];
                }
            }
        }

        int radius = 20;
        int lineLength = 15;

        // check current state of a Switch (true or false).
        if (order == 1) {
            canvas.drawCircle(zeros[0][0], h / 2, radius, paint);

            canvas.drawLine(poles[0][0] + lineLength, h / 2 + lineLength, poles[0][0] - lineLength, h / 2 - lineLength, paint);
            canvas.drawLine(poles[0][0] - lineLength, h / 2 + lineLength, poles[0][0] + lineLength, h / 2 - lineLength, paint);

        }else if(order == 2) { // && (ynorm > 0.07 || ynorm < -0.07)
            if (zeros[0][0] == zeros[1][0] &&  zeros[0][1] == zeros[1][1]){
                canvas.drawCircle(zeros[0][0], zeros[0][1], radius, paint);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawText("2", zeros[0][0] + 20, zeros[0][1]+40, paint);
                paint.setStyle(Paint.Style.STROKE);
            }else {
                canvas.drawCircle(zeros[0][0], zeros[0][1], radius, paint);
                canvas.drawCircle(zeros[1][0], zeros[1][1], radius, paint);
            }

            if (poles[0][0] == poles[1][0] &&  poles[0][1] == poles[1][1]){
                canvas.drawLine(poles[0][0] + lineLength, poles[0][1] + lineLength, poles[0][0] - lineLength, poles[0][1] - lineLength, paint);
                canvas.drawLine(poles[0][0] - lineLength, poles[0][1] + lineLength, poles[0][0] + lineLength, poles[0][1] - lineLength, paint);
                paint.setStyle(Paint.Style.FILL);
                canvas.drawText("2", poles[0][0] + 20, poles[0][1]+40, paint);
                paint.setStyle(Paint.Style.STROKE);
            }else {
                canvas.drawLine(poles[0][0] + lineLength, poles[0][1] + lineLength, poles[0][0] - lineLength, poles[0][1] - lineLength, paint);
                canvas.drawLine(poles[0][0] - lineLength, poles[0][1] + lineLength, poles[0][0] + lineLength, poles[0][1] - lineLength, paint);
                canvas.drawLine(poles[1][0] + lineLength, poles[1][1] + lineLength, poles[1][0] - lineLength, poles[1][1] - lineLength, paint);
                canvas.drawLine(poles[1][0] - lineLength, poles[1][1] + lineLength, poles[1][0] + lineLength, poles[1][1] - lineLength, paint);
            }

/*
        } else if (order == 2 ) { //&& (ynorm < 0.1 && ynorm > -0.1)

            canvas.drawCircle(zeros[0][0], h / 2, radius, paint);
            canvas.drawCircle(zeros[0][1], h / 2, radius, paint);

            canvas.drawLine(poles[0][0] + lineLength, poles[0][1] + lineLength, poles[0][0] - lineLength, poles[0][1] - lineLength, paint);
            canvas.drawLine(poles[0][0] - lineLength, poles[0][1] + lineLength, poles[0][0] + lineLength, poles[0][1] - lineLength, paint);
            canvas.drawLine(poles[0][0] + lineLength, h - poles[0][1] + lineLength, poles[0][0] - lineLength, h - poles[0][1] - lineLength, paint);
            canvas.drawLine(poles[0][0] - lineLength, h - poles[0][1] + lineLength, poles[0][0] + lineLength, h - poles[0][1] - lineLength, paint);
*/
        } else if (order > 2) {
            Paint outOffOrderColor = new Paint();
            outOffOrderColor.setColor(Color.LTGRAY);
            outOffOrderColor.setStyle(Paint.Style.FILL);
            outOffOrderColor.setStrokeWidth(1);

            canvas.drawRect(0, 0, w, h, outOffOrderColor);

            Paint text = new Paint();
            text.setColor(Color.BLACK);
            text.setTextSize(50);
            text.setStyle(Paint.Style.FILL);

            String line1 = "Only 1st & 2nd order plots";
            String line2 = "are supported at the moment";
            float line1width = text.measureText(line1, 0, line1.length()) / 2;
            float line2width = text.measureText(line2, 0, line2.length()) / 2;
            canvas.drawText(line1, w / 2 - line1width, h / 2 - 30, text);
            canvas.drawText(line2, w / 2 - line2width, h / 2 + 20, text);
        }

    }

}
