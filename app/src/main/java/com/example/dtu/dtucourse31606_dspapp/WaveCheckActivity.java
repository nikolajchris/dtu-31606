/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WaveCheckActivity extends AppCompatActivity {

    String fileChosen;      // Name of chosen wave file
    SoundProcessor sound;
    Spinner spinner;        // Spinner that holds names of all files in DTU_DSP-folder

    @Override
    protected void onCreate(Bundle savedInstanceState) {    //When activity launches:
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wave_check);

        // Spinner element
        spinner = (Spinner) findViewById(R.id.spinner);
        File folder = new File(Environment.getExternalStorageDirectory() + "/DTU_DSP/");
        File[] listOfFiles = folder.listFiles(); //Creates a list of files from folder

        List<String> categories = new ArrayList<String>(); // Spinner Drop down elements
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                categories.add(listOfFiles[i].getName());
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories); // Creating adapter for spinner
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Drop down layout style - list view with radio button
            spinner.setAdapter(dataAdapter); // attaching data adapter to spinner
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_userguide, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            try{
                Intent i = new Intent(this, WaveCheckInfoActivity.class);
                startActivity(i);
            }catch (Exception e){
                e.printStackTrace();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // This is the on-button-press case-tree for this application window
    public void waveCheckButtonTapped(View view){
        switch(view.getId()){
            case R.id.getInfo:       // extracts information about selected file
                try{
                    waveCheck();
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.Bdelete:   // deletes selected file
                try{
                    fileDelete();
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.BCheckPlay:   // Plays selected file
                try{
                    WavImport file = new WavImport();
                    fileChosen = spinner.getSelectedItem().toString();
                    sound = file.WavImport(fileChosen);
                    sound.playSound();
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }

    private void waveCheck() throws IOException {
        // Imports data from file
        fileChosen = spinner.getSelectedItem().toString();
        int infoSize = 256; //size of array, reserved for the wave-file header information.

        String filepath = Environment.getExternalStorageDirectory() + "/DTU_DSP/" + fileChosen;
        FileInputStream fin = new FileInputStream(filepath);
        DataInputStream dis = new DataInputStream(fin); // importing data from file

        byte[] wavInfo = new byte[infoSize]; //Read a small part of the file (header)
        dis.read(wavInfo, 0, wavInfo.length); //read data from file info wavInfo array
        WaveHeader header = new WaveHeader(wavInfo); //get info from bytes in array
        header.test(); // print info

        // Passes data to local identifiers
        boolean validWave = header.validWave;
        int waveLength = header.waveLength;
        int sampleRate = header.sampleRate;
        int channels = header.channels;
        int bitDepth = header.bitDepth;
        int dataSize = header.waveLength-header.headerLength;
        int duration = dataSize/(sampleRate*bitDepth/8); //in seconds

        // identifies text placement
        TextView textOutVal = (TextView) findViewById(R.id.validity);  // identifies text placement
        TextView textOutTotSize =  (TextView) findViewById(R.id.totSize);
        TextView textOutSize = (TextView) findViewById(R.id.size);
        TextView textOutSample = (TextView) findViewById(R.id.samplerate);
        TextView textOutBD = (TextView) findViewById(R.id.bitdepth);
        TextView textOutChannel = (TextView) findViewById(R.id.channels);
        TextView textOutDur= (TextView) findViewById(R.id.duration);

        if(validWave){  // if the selected file is a valif wave file
            textOutVal.setText("Verified Wave file");       // updates on-screen text
            textOutTotSize.setText(byteCalc(waveLength));
            textOutSize.setText(byteCalc(dataSize));
            textOutSample.setText(sampleRate+" samples/sec");
            textOutBD.setText(bitDepth+" bits pr. sample");

            if (duration < 1){  // if duration is less than 1 second
                textOutDur.setText("< 1 sec");
            }else{
                textOutDur.setText(duration+" sec");
            }

            if (channels == 1){
                textOutChannel.setText("Mono");
            }else if (channels == 2){
                textOutChannel.setText("Stereo");
            }else{
                textOutChannel.setText("N/A");
            }
        }else{      // if it is not a valid wave file, then post blank data fields
            textOutVal.setText("Not a valid Wave file");
            textOutTotSize.setText("---");
            textOutSize.setText("---");
            textOutSample.setText("---");
            textOutBD.setText("---");
            textOutChannel.setText("---");
            textOutDur.setText("---");
        }

    }

    private String byteCalc(int data){  // Writes file size as KB, MB or GB if needed
        String value;
        if (data >= 1000 && data < 1000000){
            value = data/1000+" KB";
        } else if (data >= 1000000 && data < 1000000000){
            value = data/1000000+" MB";
        } else if (data >= 1000000000) {
            value = data / 1000000000 + " GB";
        }else {
            value = data + " byte";
        }
        return value;
    }

    private void fileDelete(){  // deletes selected file
        fileChosen = spinner.getSelectedItem().toString();    // select file to be deleted
        File file = new File(Environment.getExternalStorageDirectory() + "/DTU_DSP/" + fileChosen); // imports file
        file.delete();              // deletes file

        finish();                   // ends activity
        startActivity(getIntent()); // restarts activity
        Toast.makeText(getApplicationContext(), fileChosen+" has been deleted", Toast.LENGTH_SHORT).show(); // on-screen message
    }


}
