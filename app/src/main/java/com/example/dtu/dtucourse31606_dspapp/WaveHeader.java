/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;


public class WaveHeader {

    public boolean validWave;
    public int headerLength;
    public int waveLength;
    public int sampleRate;
    public int channels;
    public int bitDepth;
    public int PCM;

    // Imports data from a wave file using reference layout of wave file headers
    public WaveHeader(byte[] wave){
        validWave = (wave[0] == 82 && wave[1] == 73 && wave[2] == 70 && wave[3] == 70 &&
                    wave[8] == 87 && wave[9] == 65 && wave[10] == 86 && wave[11] == 69 &&
                    wave[12] == 102 && wave[13] == 109 && wave[14] == 116 && wave[15] == 32);
        headerLength = bit8ToBit32(wave[16],wave[17],wave[18],wave[19])-16+44;
        waveLength = bit8ToBit32(wave[headerLength-4],wave[headerLength-3],wave[headerLength-2],wave[headerLength-1]);
        sampleRate = bit8ToBit32(wave[24],wave[25],wave[26],wave[27]);
        channels = bit8ToBit32(wave[22],wave[23],(byte)0,(byte)0);
        bitDepth = bit8ToBit32(wave[34],wave[35],(byte)0,(byte)0);
        PCM = bit8ToBit32(wave[20],wave[21],(byte)0,(byte)0);
    }

    // Converts 8bit values(bytes) from wave to an int consisting of 32bit
    private int bit8ToBit32(byte four, byte three, byte two, byte one){
        int temp;
        temp = (int)one;
        temp = temp << 8;
        temp = (temp & 0xFFFFFF00)|((int)two & 0xFF);
        temp = temp << 8;
        temp = (temp & 0xFFFFFF00)|((int)three & 0xFF);
        temp = temp << 8;
        temp = (temp & 0xFFFFFF00)|((int)four & 0xFF);
        return temp;
    }

    // Prints gathered information in terminal
    public void test(){
        System.out.println("Valid Wave: "+validWave);
        System.out.println("Header length: "+headerLength);
        System.out.println("Data length: "+waveLength);
        System.out.println("Sample rate: "+sampleRate);
        System.out.println("Bit depth: "+bitDepth);
        System.out.println("Channels: "+channels);
        System.out.println("PCM: "+PCM);
    }
}
