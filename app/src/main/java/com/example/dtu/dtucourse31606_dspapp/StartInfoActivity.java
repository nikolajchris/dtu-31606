/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;


public class StartInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {    //When activity launches:
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_info);


        //This function simply collects a string of text to be displayed

    }
}
