/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by Nikolaj on 25-04-2017.
 */


public class filtertab2 extends Fragment{

    static int order;

    public static LinearLayout touchLayout;
    public static ImageView poleZeroLayout;

    float x = 0;
    float y = 0;
    float xnorm;
    float ynorm;

    TextView bval;
    TextView aval;

    Switch pzSwitch;
    ImageButton zoomIn;
    ImageButton zoomOut;

    public static float[][] zeros;
    public static float[][] poles;

    EditText orderCheck;

    boolean purelyRealFirstTouch;
    boolean refresh;

    public static int h;
    public static int w;

    Display display;

    public static PoleZeroPlot plot;

    public static int zoomScale;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);

        View rootView = inflater.inflate(R.layout.filtertab2, container, false);

        zeros = new float[2][2];
        poles = new float[2][2];

        zoomScale = 3;

        purelyRealFirstTouch = true;

        zoomIn = (ImageButton) rootView.findViewById(R.id.PZzoomIn);
        zoomOut = (ImageButton) rootView.findViewById(R.id.PZzoomOut);
        poleZeroLayout = (ImageView) rootView.findViewById(R.id.poleZeroLayoutImg);
        touchLayout = (LinearLayout) rootView.findViewById(R.id.touchArea);
        touchLayout.addView(new DrawingView(getContext()));

        plot = new PoleZeroPlot();  // Initializes plotter

        pzSwitch = (Switch) rootView.findViewById(R.id.pzswitch);
        orderCheck = (EditText) getActivity().findViewById(R.id.filterOrder);


        refresh = true;

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                w = touchLayout.getWidth();
                h = touchLayout.getHeight();
            }
        });


        display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        w = size.x;
        h = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());

        // Gets the layout params that will allow you to resize the layout
        ViewGroup.LayoutParams params = touchLayout.getLayoutParams();
        plot.initPoleZeroPlot(poleZeroLayout, w, h, h);

        // Changes the height and width to the specified *pixels*
        params.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, getResources().getDisplayMetrics());
        params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, getResources().getDisplayMetrics());
        touchLayout.setLayoutParams(params);

        if(Filter_Realtime_Activity.isActive) {
            poles = Filter_Realtime_Activity.poles;
            zeros = Filter_Realtime_Activity.zeros;
            order = Filter_Realtime_Activity.order;
        }else if(Filter_Static_Activity.isActive){
            poles = Filter_Static_Activity.poles;
            zeros = Filter_Static_Activity.zeros;
            order = Filter_Static_Activity.order;
        }

        plot.drawPZ(order, poles, zeros, zoomScale);


        return rootView;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) { //if rotation changes
        Log.d("tag", "config changed");
        super.onConfigurationChanged(newConfig);

        int orientation = newConfig.orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT){ //if portrait mode
            Log.d("tag", "Portrait");
            display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            w = size.x;
            h = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());

            // Gets the layout params that will allow you to resize the layout
            ViewGroup.LayoutParams params = touchLayout.getLayoutParams();
            plot.initPoleZeroPlot(poleZeroLayout, w, h, h);
            plot.drawPZ(order, poles, zeros, zoomScale);
            poleZeroLayout.postInvalidate();

            // Changes the height and width to the specified *pixels*
            params.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, getResources().getDisplayMetrics());
            params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, getResources().getDisplayMetrics());
            touchLayout.setLayoutParams(params);
        }else if (orientation == Configuration.ORIENTATION_LANDSCAPE) { // if landscape mode
            Log.d("tag", "Landscape");
            display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            w = size.x;
            h = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 250, getResources().getDisplayMetrics());

            // Gets the layout params that will allow you to resize the layout
            ViewGroup.LayoutParams params = touchLayout.getLayoutParams();
            plot.initPoleZeroPlot(poleZeroLayout, w, h, h);
            plot.drawPZ(order, poles, zeros, zoomScale);
            poleZeroLayout.postInvalidate();

            // Changes the height and width to the specified *pixels*
            params.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, getResources().getDisplayMetrics());
            params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, h, getResources().getDisplayMetrics());
            touchLayout.setLayoutParams(params);
        }
        else
            Log.w("tag", "other: " + orientation);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(zoomScale < 30) {
                    zoomScale++;
                    plot.drawPZ(order, poles, zeros, zoomScale);
                    poleZeroLayout.invalidate();
                }
            }
        });

        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(zoomScale > 2){
                    zoomScale--;
                    plot.drawPZ(order, poles, zeros, zoomScale);
                    poleZeroLayout.invalidate();
                }
            }
        });

    }

    public class DrawingView extends View {
        public DrawingView(Context context) {
            super(context);
        }

        public boolean onTouchEvent(MotionEvent event) {
            bval = (TextView) getActivity().findViewById(R.id.B);
            aval = (TextView) getActivity().findViewById(R.id.A);
            order = 1;

            orderCheck = (EditText) getActivity().findViewById(R.id.filterOrder);
            if(orderCheck != null) {
                String orderInput = orderCheck.getText().toString();  // Get string from users input
                if (!orderInput.equals("") && !orderInput.equals("0")) {                 // if string is not empty
                    order = Integer.parseInt(orderInput);   // Parse the string to an int named "order"
                } else {                                      // if empty input
                    order = 1;
                    TextView ord = (TextView) getActivity().findViewById(R.id.filterOrder);
                    ord.setText(String.valueOf(order));
                }
            }

            // Formats decimal-numbers correctly to the 3rd decimal and uses dot notation instead of comma
            DecimalFormat decOut = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
            if (event.getAction() == MotionEvent.ACTION_DOWN && order <= 2) {
                //order = Integer.parseInt(((EditText) getActivity().findViewById(R.id.filterOrderStatic)).getText().toString());
                x = event.getX();
                y = event.getY();

                xnorm = (x-(w/2))/(plot.radius); //Normalizing to fit plot center
                ynorm = -(y-(h/2))/(plot.radius);

                float[][] coef = new float[order][2];

                for (int i = 0; i < order; i++) {
                    for (int j = 0; j < 2; j++) {
                        if(i == 0){
                            if(j==0){
                                coef[i][j] = xnorm;
                            }else{
                                coef[i][j] = ynorm;
                            }
                        } else{
                            if(j==0){
                                coef[i][j] = xnorm;
                            }else{
                                coef[i][j] = -ynorm;
                            }
                        }
                    }
                }

                Boolean pzState = pzSwitch.isChecked();
                if (pzState == false) {

                    zeros = new float[order][2];

                    if (order == 1) {
                        double[] values = calcCoef(coef, order);
                        bval.setText(String.valueOf(decOut.format(values[0])) + "," + String.valueOf(decOut.format(values[1])));
                        zeros[0][0] = x-w/2;
                        zeros[0][1] = y-h/2;

                    } else if (order == 2) {
                        double[] values = calcCoef(coef, order);
                        bval.setText(String.valueOf(decOut.format(values[0])) + "," + String.valueOf(decOut.format(values[1])) + "," + String.valueOf(decOut.format(values[2])));
                        zeros[0][0] = x-w/2;
                        zeros[0][1] = y-h/2;
                        zeros[1][0] = zeros[0][0];
                        zeros[1][1] = -zeros[0][1];
                        purelyRealFirstTouch = true;

                    } else if (order == 2 && Math.abs(ynorm) < 0.5){
                        if (purelyRealFirstTouch){
                            zeros[0][0] = x-w/2;
                            zeros[0][1] = 0;
                            zeros[1][0] = zeros[0][0];
                            zeros[1][1] = zeros[0][1];
                        }else{
                            zeros[1][0] = zeros[0][0];
                            zeros[1][1] = zeros[0][1];
                            zeros[0][0] = x-w/2;
                            zeros[0][1] = 0;
                        }
                    }

                    if(Filter_Realtime_Activity.isActive){
                        Filter_Realtime_Activity.zeros = zeros;
                        poles = Filter_Realtime_Activity.poles;
                    }else if(Filter_Static_Activity.isActive){
                        Filter_Static_Activity.zeros = zeros;
                        poles = Filter_Static_Activity.poles;
                    }

                } else {

                    poles = new float[order][2];

                    if (order == 1) {
                        double[] values = calcCoef(coef, order);
                        aval.setText(String.valueOf(decOut.format(values[1])));
                        poles[0][0] = x-w/2;
                        poles[0][1] = y-h/2;

                    } else if (order == 2) {
                        double[] values = calcCoef(coef, order);
                        aval.setText(String.valueOf(decOut.format(values[1])) + "," + String.valueOf(decOut.format(values[2])));
                        poles[0][0] = x-w/2;
                        poles[0][1] = y-h/2;
                        poles[1][0] = poles[0][0];
                        poles[1][1] = -poles[0][1];
                        purelyRealFirstTouch = true;

                    }else if (order == 2 && Math.abs(ynorm) < 0.5){
                        if (purelyRealFirstTouch){
                            poles[0][0] = x-w/2;
                            poles[0][1] = 0;
                            poles[1][0] = poles[0][0];
                            poles[1][1] = poles[0][1];
                        }else{
                            poles[1][0] = poles[0][0];
                            poles[1][1] = poles[0][1];
                            poles[0][0] = x-w/2;
                            poles[0][1] = 0;
                        }
                    }

                    if(Filter_Realtime_Activity.isActive){
                        Filter_Realtime_Activity.poles = poles;
                        zeros = Filter_Realtime_Activity.zeros;
                    }else if(Filter_Static_Activity.isActive){
                        Filter_Static_Activity.poles = poles;
                        zeros = Filter_Static_Activity.zeros;
                    }
                }
                plot.drawPZ(order, poles, zeros, zoomScale);
                poleZeroLayout.invalidate();
            }
            return false;
        }
    }

    private double[] calcCoef(float[][] z, int ord) {
        double[] coef = new double[ord+1];

        if (order == 1) {                 // If the equation is a first order equation
            float x = z[0][0];
            double a = 1;
            double b = -x;
            coef[0] = a;
            coef[1] = b;
        }

        if (order == 2) { // If the equation is a second order equation, solving a Quadratic equation
            double Re1 = z[0][0];
            double Im1 = z[0][1];
            double Re2 = z[1][0];
            double Im2 = z[1][1];

            // Using Vieta's formula
            double a = 1;
            double b = -(Re1+Re2); // (x1 + x2) = -b/a
            double c = (Re1*Re2)+Math.abs(Im1*Im2); // x1*x2 = c/a

            coef[0] = a;
            coef[1] = b;
            coef[2] = c;

        }
        return coef;
    }
}
