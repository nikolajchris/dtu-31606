/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;

import java.io.IOException;

/**
 * Created by Nikolaj on 25-04-2017.
 */

public class filtertab3 extends Fragment {
    double[] a;
    double[] b;
    int order;
    int fs;
    int fmin;
    int fmax;

    public static ImageView magnitudeLayout;
    public static ImageView phaseLayout;
    public static PlotFrequencyResponse filterResponsePlot;
    public static Switch linLogSwitch;

    SoundProcessor filterResponse;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);

        final View rootView = inflater.inflate(R.layout.filtertab3, container, false);

        linLogSwitch = (Switch) rootView.findViewById(R.id.linLogSwitch);

        magnitudeLayout = (ImageView) rootView.findViewById(R.id.MagPlotArea);
        phaseLayout = (ImageView) rootView.findViewById(R.id.PhaPlotArea);

        filterResponsePlot = new PlotFrequencyResponse();  // Initializes plotter

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int w = size.x;
        int h = size.y;

        if(Filter_Realtime_Activity.isActive) {
            a = Filter_Realtime_Activity.a;
            b = Filter_Realtime_Activity.b;
            order = Filter_Realtime_Activity.order;
            fs = Filter_Realtime_Activity.fs;
            fmin = Filter_Realtime_Activity.fmin;
            fmax = Filter_Realtime_Activity.fs/2;
            filterResponse = Filter_Realtime_Activity.filterResponse;

            if (w < h) {
                filterResponsePlot.initBodePlot(magnitudeLayout, phaseLayout, w, w * 3 / 4, fmin, fmax);
            } else {
                filterResponsePlot.initBodePlot(magnitudeLayout, phaseLayout, h, h * 3 / 4, fmin, fmax);
            }

            try {
                Filter_Realtime_Activity.plotFilterResponse();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            linLogSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try {
                        Filter_Realtime_Activity.plotFilterResponse();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        else if(Filter_Static_Activity.isActive){
            a = Filter_Static_Activity.a;
            b = Filter_Static_Activity.b;
            order = Filter_Static_Activity.order;
            fs = Filter_Static_Activity.fs;
            fmin = Filter_Static_Activity.fmin;
            fmax = Filter_Static_Activity.fs/2;
            filterResponse = Filter_Static_Activity.filterResponse;

            if (w < h) {
                filterResponsePlot.initBodePlot(magnitudeLayout, phaseLayout, w, w * 3 / 4, fmin, fmax);
            } else {
                filterResponsePlot.initBodePlot(magnitudeLayout, phaseLayout, h, h * 3 / 4, fmin, fmax);
            }

            try {
                Filter_Static_Activity.plotFilterResponse();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            linLogSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try {
                        Filter_Static_Activity.plotFilterResponse();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        filterResponsePlot.drawBodePlotWindow(linLogSwitch.isChecked(), 60, 90, false);
        return rootView;

    }

}
