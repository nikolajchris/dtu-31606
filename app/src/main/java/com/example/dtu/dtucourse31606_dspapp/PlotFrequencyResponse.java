/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.widget.ImageView;

/**
 * Created by Nikolaj Christiansen on 06-04-2017.
 */

public class PlotFrequencyResponse extends Filter_Realtime_Activity {

    public double[] data;                   // Hold PlotFrequencyResponse data
    public double[] unfilteredData;                   // Hold PlotFrequencyResponse data
    ImageView magnitudeLayout;                 // Graph-window in use for plots
    ImageView phaseLayout;
    Bitmap bmpMag;
    Bitmap bmpPha;
    int wmax;
    int hmax;
    float scalar;
    Canvas canvasMag;
    Canvas canvasPha;

    float x0;
    float xmax;
    float y0;
    float ymax;
    float plotWidth;
    float plotHeight;

    int bigText = 40;
    int smallText = 30;

    double[] a;
    double[] b;
    int order;

    float dBmax;
    float dBmin;
    float dBspan;

    float phaMax = 0;
    float phaMin = 0;
    float phaSpan = 0;

    int fmin;
    int fmax;

    double[][] magnitudePlotData;
    double[][] phasePlotData;
    double[][] unfilteredPlotData;
    boolean firstRun;


    // Takes in the PlotFrequencyResponse data, sample rate, PlotFrequencyResponse-window, filename and color-number based on presses
    public void initMagnitudeSpectrum(ImageView plotWindow, int width, int height, int freqmin, int freqmax) {
        magnitudeLayout = plotWindow;

        wmax = width;
        hmax = height;
        scalar = 0.1f;

        x0 = wmax * scalar;
        xmax = wmax;
        y0 = 0 + hmax * scalar/ 2;
        ymax = hmax - hmax * 0.15f;
        plotWidth = (xmax - x0);
        plotHeight = (ymax - y0); // hmax * 0.15f/ 2

        bmpMag = Bitmap.createBitmap(wmax, hmax, Bitmap.Config.ARGB_8888);
        canvasMag = new Canvas(bmpMag);
        magnitudeLayout.setImageBitmap(bmpMag);

        a = Filter_Realtime_Activity.a;
        b = Filter_Realtime_Activity.b;
        order = Filter_Realtime_Activity.order;

        dBmax = 0;
        dBmin = 0;

        fmin = freqmin;
        fmax = freqmax;

        firstRun = true;

    }

    // Takes in the PlotFrequencyResponse data, sample rate, PlotFrequencyResponse-window, filename and color-number based on presses
    public void initBodePlot(ImageView magnitudePlot, ImageView phasePlot, int width, int height, int freqmin, int freqmax) {
        magnitudeLayout = magnitudePlot;
        phaseLayout = phasePlot;

        wmax = width;
        hmax = height;
        scalar = 0.1f;

        x0 = wmax * scalar;
        xmax = wmax;
        y0 = 0 + hmax * scalar/ 2;
        ymax = hmax - hmax * 0.15f;
        plotWidth = (xmax - x0);
        plotHeight = (ymax - y0); // hmax * 0.15f/ 2

        bmpMag = Bitmap.createBitmap(wmax, hmax, Bitmap.Config.ARGB_8888);
        canvasMag = new Canvas(bmpMag);
        magnitudeLayout.setImageBitmap(bmpMag);

        bmpPha = Bitmap.createBitmap(wmax, hmax, Bitmap.Config.ARGB_8888);
        canvasPha = new Canvas(bmpPha);
        phaseLayout.setImageBitmap(bmpPha);

        //a = Filter_Realtime_Activity.a;
        //b = Filter_Realtime_Activity.b;
        //order = Filter_Realtime_Activity.order;

        dBmax = 0;
        dBmin = 0;

        fmin = freqmin;
        fmax = freqmax;

        firstRun = true;

    }

    private void magnitudeSpectrumSetup(){
        canvasMag.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        //SETUP AXIS
        Paint edgeColor = new Paint();
        edgeColor.setColor(Color.BLACK);
        edgeColor.setStyle(Paint.Style.FILL_AND_STROKE);
        edgeColor.setStrokeWidth(2);

        canvasMag.drawLine(x0,y0,x0,ymax,edgeColor);
        canvasMag.drawLine(xmax-1,y0,xmax-1,ymax,edgeColor);
        canvasMag.drawLine(x0,ymax,xmax,ymax,edgeColor);
        canvasMag.drawLine(x0,y0,xmax,y0,edgeColor);

        // DRAWING LABELS ON COORDINATE SYSTEM
        Paint text = new Paint();
        text.setColor(Color.BLACK);
        text.setTextSize(bigText);
        text.setStyle(Paint.Style.FILL);

        String freq = "Frequency [Hz]";
        String db = "Magnitude [dB FS]";

        canvasMag.drawText(freq, plotWidth/2+x0-text.measureText(freq, 0, freq.length())/2, hmax-y0/2, text);
        canvasMag.rotate(-90,hmax/2,hmax/2);
        canvasMag.drawText(db, hmax/2+y0-text.measureText(db, 0, db.length())/2, 30, text);
        canvasMag.rotate(90,hmax/2,hmax/2);
    }

    private void bodePlotSetup(boolean showPhase){
        //SETUP AXIS
        Paint edgeColor = new Paint();
        edgeColor.setColor(Color.BLACK);
        edgeColor.setStyle(Paint.Style.FILL_AND_STROKE);
        edgeColor.setStrokeWidth(2);

        // DRAWING LABELS ON COORDINATE SYSTEM
        Paint text = new Paint();
        text.setColor(Color.BLACK);
        text.setTextSize(bigText);
        text.setStyle(Paint.Style.FILL);

        String freq = "Frequency [Hz]";
        String db = "Magnitude [dB FS]";
        String phase = "Phase [deg]";

        canvasMag.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        canvasMag.drawLine(x0,y0,x0,ymax,edgeColor);
        canvasMag.drawLine(xmax-1,y0,xmax-1,ymax,edgeColor);
        canvasMag.drawLine(x0,ymax,xmax,ymax,edgeColor);
        canvasMag.drawLine(x0,y0,xmax,y0,edgeColor);

        canvasMag.drawText(freq, plotWidth/2+x0-text.measureText(freq, 0, freq.length())/2, hmax-y0/2, text);
        canvasMag.rotate(-90,hmax/2,hmax/2);
        canvasMag.drawText(db, hmax/2+y0-text.measureText(db, 0, db.length())/2, 30, text);
        canvasMag.rotate(90,hmax/2,hmax/2);

        if(showPhase) {
            canvasPha.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

            canvasPha.drawLine(x0,y0,x0,ymax,edgeColor);
            canvasPha.drawLine(xmax-1,y0,xmax-1,ymax,edgeColor);
            canvasPha.drawLine(x0,ymax,xmax,ymax,edgeColor);
            canvasPha.drawLine(x0,y0,xmax,y0,edgeColor);

            canvasPha.drawText(freq, plotWidth/2+x0-text.measureText(freq, 0, freq.length())/2, hmax-y0/2, text);
            canvasPha.rotate(-90,hmax/2,hmax/2);
            canvasPha.drawText(phase, hmax/2+y0-text.measureText(db, 0, db.length())/2, 30, text);
            canvasPha.rotate(90,hmax/2,hmax/2);
        }else{
            canvasPha.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        }
    }

    public void drawMagnitudeSpectrumWindow(boolean linScale, int dynamicRange){
        canvasMag.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        Paint tickColor = new Paint();
        tickColor.setColor(Color.BLACK);
        tickColor.setStyle(Paint.Style.FILL);
        tickColor.setStrokeWidth(3);

        Paint gridColor = new Paint();
        gridColor.setColor(Color.GRAY);
        gridColor.setStyle(Paint.Style.FILL);
        gridColor.setStrokeWidth(1);

        Paint gridMinorColor = new Paint();
        gridMinorColor.setColor(Color.LTGRAY);
        gridMinorColor.setStyle(Paint.Style.FILL);
        gridMinorColor.setStrokeWidth(1);

        Paint text = new Paint();
        text.setColor(Color.BLACK);
        text.setTextSize(smallText);
        text.setStyle(Paint.Style.FILL);

        if (linScale == true){
            magnitudeSpectrumSetup();
            text.setTextSize(smallText);
            float[] gridLin = new float[fmax-fmin];
            int[] freqVal = new int[fmax-fmin];
            for(int i = 0; i <= (fmax-fmin); i = i+1000){
                if (i == 0){
                    freqVal[1] = 1;
                    gridLin[1] = 1/((float)gridLin.length)*plotWidth;
                    canvasMag.drawLine(gridLin[1] + x0, y0+1, gridLin[1] + x0, ymax, gridColor);
                    canvasMag.drawText("0", gridLin[i] + x0 - x0/10, ymax + y0, text);
                } if (i > 0){
                    freqVal[i] = i;
                    gridLin[i] = i/((float)gridLin.length)*plotWidth;
                    canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i]+ x0, ymax, gridColor);
                }
            }

            for(int i = 0; i <= (fmax-fmin); i = i+5000){
                if (i > 0){
                    freqVal[i] = i;
                    gridLin[i] = i/((float)gridLin.length)*plotWidth;
                    canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i]+ x0, ymax, gridColor);
                    canvasMag.drawText(i/1000+"k", gridLin[i] + x0 - x0/5, ymax + y0, text);
                    canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i] + x0, y0+y0/3, tickColor);
                    canvasMag.drawLine(gridLin[i] + x0, ymax, gridLin[i] + x0, ymax-y0/3, tickColor);
                }
            }
        }else{
            magnitudeSpectrumSetup();
            text.setTextSize(smallText);
            float[] gridCoorLog = new float[fmax-fmin];
            int[] freqVal = new int[fmax-fmin];
            float[] gridMinorLog = new float[fmax-fmin];
            for(int i = 1; i < fmax; i = i*10){
                gridCoorLog[i] = (float)(Math.log10(i)/Math.log10(fmax/fmin)*plotWidth);
                freqVal[i] = i*fmin;
                for (int j = 1; j <= 9; j++){
                    gridMinorLog[i+j] = (float)(Math.log10(i*j)/Math.log10(fmax/fmin)*plotWidth);
                }
            }

            for(int i = 1; i < gridCoorLog.length; i*=10) {
                for (int j = 1; j <= 9; j++) {
                    canvasMag.drawLine(gridMinorLog[i + j] + x0, y0+1, gridMinorLog[i + j] + x0, ymax, gridMinorColor);
                }
                canvasMag.drawLine(gridCoorLog[i] + x0, y0+1, gridCoorLog[i] + x0, ymax, gridColor);
                if (i == 1) {
                    String num = Integer.toString(freqVal[i]);
                    canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                }
                if (i > 1) {
                    if (freqVal[i] >= 1000) {
                        String num = Integer.toString(freqVal[i] / 1000) + "k";
                        canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                    } else {
                        String num = Integer.toString(freqVal[i]);
                        canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                    }
                    canvasMag.drawLine(gridCoorLog[i] + x0, y0+1, gridCoorLog[i] + x0, y0 + y0 / 3, tickColor);
                    canvasMag.drawLine(gridCoorLog[i] + x0, ymax, gridCoorLog[i] + x0, ymax - y0 / 3, tickColor);
                }
            }
        }

        for (int i = 0; i < dynamicRange; i += 10){
            canvasMag.drawLine(x0+1, i*plotHeight/dynamicRange+y0, xmax-1, i*plotHeight/dynamicRange+y0, gridColor);
            String number = String.valueOf(-i);
            canvasMag.drawText(number, x0*13/20-text.measureText(number, 0, number.length())/2, i*plotHeight/dynamicRange+y0+10, text);
        }
    }

    public void drawBodePlotWindow(boolean linScale, int dynamicRange, int phaseSpan, boolean showPhase){
        canvasMag.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        canvasPha.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        Paint tickColor = new Paint();
        tickColor.setColor(Color.BLACK);
        tickColor.setStyle(Paint.Style.FILL);
        tickColor.setStrokeWidth(3);

        Paint gridColor = new Paint();
        gridColor.setColor(Color.GRAY);
        gridColor.setStyle(Paint.Style.FILL);
        gridColor.setStrokeWidth(1);

        Paint gridMinorColor = new Paint();
        gridMinorColor.setColor(Color.LTGRAY);
        gridMinorColor.setStyle(Paint.Style.FILL);
        gridMinorColor.setStrokeWidth(1);

        Paint text = new Paint();
        text.setColor(Color.BLACK);
        text.setTextSize(smallText);
        text.setStyle(Paint.Style.FILL);

        if(showPhase){
            if (linScale == true){
                bodePlotSetup(true);
                text.setTextSize(smallText);
                float[] gridLin = new float[fmax-fmin];
                int[] freqVal = new int[fmax-fmin];
                for(int i = 0; i <= (fmax-fmin); i = i+1000){
                    if (i == 0){
                        freqVal[1] = 1;
                        gridLin[1] = 1/((float)gridLin.length)*plotWidth;
                        canvasMag.drawLine(gridLin[1] + x0, y0+1, gridLin[1] + x0, ymax, gridColor);
                        canvasMag.drawText("0", gridLin[i] + x0 - x0/10, ymax + y0, text);
                        canvasPha.drawLine(gridLin[1] + x0, y0+1, gridLin[1] + x0, ymax, gridColor);
                        canvasPha.drawText("0", gridLin[i] + x0 - x0/10, ymax + y0, text);
                    } if (i > 0){
                        freqVal[i] = i;
                        gridLin[i] = i/((float)gridLin.length)*plotWidth;
                        canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i]+ x0, ymax, gridColor);
                        canvasPha.drawLine(gridLin[i] + x0, y0+1, gridLin[i]+ x0, ymax, gridColor);
                    }
                }

                for(int i = 0; i <= (fmax-fmin); i = i+5000){
                    if (i > 0){
                        freqVal[i] = i;
                        gridLin[i] = i/((float)gridLin.length)*plotWidth;
                        canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i]+ x0, ymax, gridColor);
                        canvasMag.drawText(i/1000+"k", gridLin[i] + x0 - x0/5, ymax + y0, text);
                        canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i] + x0, y0+y0/3, tickColor);
                        canvasMag.drawLine(gridLin[i] + x0, ymax, gridLin[i] + x0, ymax-y0/3, tickColor);

                        canvasPha.drawLine(gridLin[i] + x0, y0+1, gridLin[i]+ x0, ymax, gridColor);
                        canvasPha.drawText(i/1000+"k", gridLin[i] + x0 - x0/5, ymax + y0, text);
                        canvasPha.drawLine(gridLin[i] + x0, y0+1, gridLin[i] + x0, y0+y0/3, tickColor);
                        canvasPha.drawLine(gridLin[i] + x0, ymax, gridLin[i] + x0, ymax-y0/3, tickColor);
                    }
                }
            }else{
                bodePlotSetup(true);
                text.setTextSize(smallText);
                float[] gridCoorLog = new float[fmax-fmin];
                int[] freqVal = new int[fmax-fmin];
                float[] gridMinorLog = new float[fmax-fmin];
                for(int i = 1; i < fmax; i = i*10){
                    gridCoorLog[i] = (float)(Math.log10(i)/Math.log10(fmax/fmin)*plotWidth);
                    freqVal[i] = i*fmin;
                    for (int j = 1; j <= 9; j++){
                        gridMinorLog[i+j] = (float)(Math.log10(i*j)/Math.log10(fmax/fmin)*plotWidth);
                    }
                }

                for(int i = 1; i < gridCoorLog.length; i*=10) {
                    for (int j = 1; j <= 9; j++) {
                        canvasMag.drawLine(gridMinorLog[i + j] + x0, y0+1, gridMinorLog[i + j] + x0, ymax, gridMinorColor);
                        canvasPha.drawLine(gridMinorLog[i + j] + x0, y0+1, gridMinorLog[i + j] + x0, ymax, gridMinorColor);
                    }
                    canvasMag.drawLine(gridCoorLog[i] + x0, y0+1, gridCoorLog[i] + x0, ymax, gridColor);
                    canvasPha.drawLine(gridCoorLog[i] + x0, y0+1, gridCoorLog[i] + x0, ymax, gridColor);
                    if (i == 1) {
                        String num = Integer.toString(freqVal[i]);
                        canvasPha.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                        canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                    }
                    if (i > 1) {
                        if (freqVal[i] >= 1000) {
                            String num = Integer.toString(freqVal[i] / 1000) + "k";
                            canvasPha.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                            canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                        } else {
                            String num = Integer.toString(freqVal[i]);
                            canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                            canvasPha.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);

                        }
                        canvasMag.drawLine(gridCoorLog[i] + x0, y0+1, gridCoorLog[i] + x0, y0 + y0 / 3, tickColor);
                        canvasMag.drawLine(gridCoorLog[i] + x0, ymax, gridCoorLog[i] + x0, ymax - y0 / 3, tickColor);
                        canvasPha.drawLine(gridCoorLog[i] + x0, y0+1, gridCoorLog[i] + x0, y0 + y0 / 3, tickColor);
                        canvasPha.drawLine(gridCoorLog[i] + x0, ymax, gridCoorLog[i] + x0, ymax - y0 / 3, tickColor);
                    }
                }
            }

            for (int i = 0; i < dynamicRange; i += 10){
                canvasMag.drawLine(x0+1, i*plotHeight/dynamicRange+y0, xmax-1, i*plotHeight/dynamicRange+y0, gridColor);
                String number = String.valueOf(-i);
                canvasMag.drawText(number, x0*13/20-text.measureText(number, 0, number.length())/2, i*plotHeight/dynamicRange+y0+10, text);
            }


            for (int i = 0; i < phaseSpan; i += phaseSpan/5){
                canvasPha.drawLine(x0+1, i*plotHeight/phaseSpan+y0, xmax-1, i*plotHeight/phaseSpan+y0, gridColor);
                String number = String.valueOf(-i);
                canvasPha.drawText(number, x0*13/20-text.measureText(number, 0, number.length())/2, i*plotHeight/phaseSpan+y0+10, text);
            }
        }else{
            if (linScale == true){
                bodePlotSetup(false);
                text.setTextSize(smallText);
                float[] gridLin = new float[fmax-fmin];
                int[] freqVal = new int[fmax-fmin];
                for(int i = 0; i <= (fmax-fmin); i = i+1000){
                    if (i == 0){
                        freqVal[1] = 1;
                        gridLin[1] = 1/((float)gridLin.length)*plotWidth;
                        canvasMag.drawLine(gridLin[1] + x0, y0+1, gridLin[1] + x0, ymax, gridColor);
                        canvasMag.drawText("0", gridLin[i] + x0 - x0/10, ymax + y0, text);
                    } if (i > 0){
                        freqVal[i] = i;
                        gridLin[i] = i/((float)gridLin.length)*plotWidth;
                        canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i]+ x0, ymax, gridColor);
                    }
                }

                for(int i = 0; i <= (fmax-fmin); i = i+5000){
                    if (i > 0){
                        freqVal[i] = i;
                        gridLin[i] = i/((float)gridLin.length)*plotWidth;
                        canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i]+ x0, ymax, gridColor);
                        canvasMag.drawText(i/1000+"k", gridLin[i] + x0 - x0/5, ymax + y0, text);
                        canvasMag.drawLine(gridLin[i] + x0, y0+1, gridLin[i] + x0, y0+y0/3, tickColor);
                        canvasMag.drawLine(gridLin[i] + x0, ymax, gridLin[i] + x0, ymax-y0/3, tickColor);
                    }
                }
            }else{
                bodePlotSetup(false);
                text.setTextSize(smallText);
                float[] gridCoorLog = new float[fmax-fmin];
                int[] freqVal = new int[fmax-fmin];
                float[] gridMinorLog = new float[fmax-fmin];
                for(int i = 1; i < fmax; i = i*10){
                    gridCoorLog[i] = (float)(Math.log10(i)/Math.log10(fmax/fmin)*plotWidth);
                    freqVal[i] = i*fmin;
                    for (int j = 1; j <= 9; j++){
                        gridMinorLog[i+j] = (float)(Math.log10(i*j)/Math.log10(fmax/fmin)*plotWidth);
                    }
                }

                for(int i = 1; i < gridCoorLog.length; i*=10) {
                    for (int j = 1; j <= 9; j++) {
                        canvasMag.drawLine(gridMinorLog[i + j] + x0, y0+1, gridMinorLog[i + j] + x0, ymax, gridMinorColor);
                    }
                    canvasMag.drawLine(gridCoorLog[i] + x0, y0+1, gridCoorLog[i] + x0, ymax, gridColor);
                    if (i == 1) {
                        String num = Integer.toString(freqVal[i]);
                        canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                    }
                    if (i > 1) {
                        if (freqVal[i] >= 1000) {
                            String num = Integer.toString(freqVal[i] / 1000) + "k";
                            canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                        } else {
                            String num = Integer.toString(freqVal[i]);
                            canvasMag.drawText(num, gridCoorLog[i] + x0-text.measureText(num, 0, num.length())/2 , ymax + y0, text);
                        }
                        canvasMag.drawLine(gridCoorLog[i] + x0, y0+1, gridCoorLog[i] + x0, y0 + y0 / 3, tickColor);
                        canvasMag.drawLine(gridCoorLog[i] + x0, ymax, gridCoorLog[i] + x0, ymax - y0 / 3, tickColor);
                    }
                }
            }

            for (int i = 0; i < dynamicRange; i += 10){
                canvasMag.drawLine(x0+1, i*plotHeight/dynamicRange+y0, xmax-1, i*plotHeight/dynamicRange+y0, gridColor);
                String number = String.valueOf(-i);
                canvasMag.drawText(number, x0*13/20-text.measureText(number, 0, number.length())/2, i*plotHeight/dynamicRange+y0+10, text);
            }
        }

    }

    public void drawBodePlotData(double[][] complexFFT, boolean linLogScale, boolean showPhase) {
        canvasMag.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        canvasPha.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        Paint text = new Paint();
        text.setColor(Color.BLACK);
        text.setTextSize(smallText);
        text.setStyle(Paint.Style.FILL);

        Paint cutoffColor = new Paint();
        cutoffColor.setColor(Color.parseColor("#306A9F"));
        cutoffColor.setStyle(Paint.Style.FILL);
        cutoffColor.setStrokeWidth(2);
        cutoffColor.setTextSize(smallText);


        if(showPhase){
            if(firstRun == true) {
                dBmin = 0;
                phaMin = 0;

                if(Filter_Realtime_Activity.isActive) {
                    if(a != Filter_Realtime_Activity.a || b != Filter_Realtime_Activity.b || order != Filter_Realtime_Activity.order) {
                        firstRun = true;
                        a = Filter_Realtime_Activity.a;
                        b = Filter_Realtime_Activity.b;
                    }
                }else if(Filter_Static_Activity.isActive){
                    if(a != Filter_Static_Activity.a || b != Filter_Static_Activity.b || order != Filter_Static_Activity.order) {
                        firstRun = true;
                        a = Filter_Static_Activity.a;
                        b = Filter_Static_Activity.b;
                    }
                }

                double[] magTemp = new double[complexFFT.length];
                double[] phaseTemp = new double[complexFFT.length];

                for(int j=0; j < magTemp.length; j++){
                    magTemp[j] = Math.sqrt(Math.pow(complexFFT[j][0],2)+Math.pow(complexFFT[j][1],2)); // Finding the magnitude of the transformed complex signal
                }

                for( int i = 0; i < phaseTemp.length; i++) {
                    phaseTemp[i] = Math.atan2(Math.sin(magTemp[i]),Math.cos(magTemp[i]));
                }

                double[] phaseUnwrapped = phaseTemp;

            /*
            double[] phaseUnwrapped = new double[phaseTemp.length];

            int k = 0;				// initialize k to 0
            int kmax = 0;
            int kmin = 0;
            boolean first = true;

            for (int i = 0;  i < phaseTemp.length-1; i++) {
                phaseUnwrapped[i] = phaseTemp[i] + (2 * Math.PI * k); // add 2 * pi * k
                if (Math.abs(phaseTemp[i+1]-phaseTemp[i]) > Math.PI) {  // if diff is greater than  alpha, increment or decrement k
                    if (phaseTemp[i + 1] < phaseTemp[i]) { // if the phase jump is negative, increment k
                        k = k + 1;
                    } else {           // if the phase jump is positive, decrement k
                        k = k - 1;
                    }

                    if(k > kmax){
                        kmax = k;
                        if(first){
                            kmin = k;
                            first = false;
                        }
                    }

                    if (k < kmin){
                        kmin = k;
                    }
                }
                phaseUnwrapped[i+1] = phaseTemp[i+1]+(2*Math.PI*k); // add 2*pi*k to the last element of the input
            }

            double phaseSpan = (kmax-Math.abs(kmin))*2*Math.PI;
            */


                int dotsPrPlot = (int) plotWidth; //512;
                int scale1 = magTemp.length / dotsPrPlot;    // Plots with a resolution of 256 points pr. PlotFrequencyResponse
                int scale2 = phaseUnwrapped.length / dotsPrPlot;    // Plots with a resolution of 256 points pr. PlotFrequencyResponse
                double[] temp1 = new double[(magTemp.length/(scale1*2))];  //Same scale algorithm as in frequency-domain-PlotFrequencyResponse
                double[] temp2 = new double[(phaseTemp.length/(scale2*2))];  //Same scale algorithm as in frequency-domain-PlotFrequencyResponse
                magnitudePlotData = new double[temp1.length][2];
                phasePlotData = new double[temp2.length][2];

                double sum = 0;
                double maxVal = 0;

                for (int j = 0; j < temp1.length; j++) {
                    for (int l = 0; l < scale1; l++) {
                        sum += magTemp[j * scale1 + l];
                    }
                    sum /= scale1;
                    if (sum > maxVal) {
                        maxVal = sum;
                    }
                    temp1[j] = sum;
                    sum = 0;
                }

                for (int j = 0; j < temp2.length; j++) {
                    for (int l = 0; l < scale2; l++) {
                        sum += phaseUnwrapped[j * scale2 + l];
                    }
                    sum /= scale2;
                    temp2[j] = sum;
                    sum = 0;
                }



                for (int j = 0; j < temp1.length; j++) {
                    // Normalizes the x-axis from 0 to 1
                    // and finds the dB-value of the signal the maxVal as reference value
                    magnitudePlotData[j][0] = (float) j;
                    magnitudePlotData[j][1] = 20 * Math.log10(temp1[j] / maxVal);
                    phasePlotData[j][0] = (float) j;
                    phasePlotData[j][1] = Math.toDegrees(temp2[j]);

                    if (magnitudePlotData[j][1] > dBmax) {
                        dBmax = (float) magnitudePlotData[j][1];
                    }
                    if (j == 0) {
                        dBmin = (float) magnitudePlotData[j][1];
                    }
                    if (magnitudePlotData[j][1] < dBmin) { // If sum are smaller then previous minVal, then new minVal
                        dBmin = (float) magnitudePlotData[j][1];
                    }


                    if (phasePlotData[j][1] > phaMax) {
                        phaMax = (float) phasePlotData[j][1];
                    }
                    if (j == 0) {
                        phaMin = (float) phasePlotData[j][1];
                    }
                    if (phasePlotData[j][1] < phaMin) { // If sum are smaller then previous minVal, then new minVal
                        phaMin = (float) phasePlotData[j][1];
                    }
                }
                dBspan = dBmax - dBmin;
                phaSpan = phaMax - phaMin;

                firstRun = false;
            }

            drawBodePlotWindow(linLogScale, (int) dBspan, (int) phaSpan, showPhase);

            Paint pointColor = new Paint();
            pointColor.setColor(Color.parseColor("#990000"));
            pointColor.setStyle(Paint.Style.FILL);
            pointColor.setStrokeWidth(3);

            float xLinearScale = plotWidth / magnitudePlotData.length;
            float yscale = dBspan / plotHeight;
            float phaseYscale = phaSpan/plotHeight;
            int cutoff = findCutoffPoint(magnitudePlotData);


            if (linLogScale == true) {
                canvasMag.drawLine(x0+1, (float) (-magnitudePlotData[cutoff][1] / yscale + y0), xmax-1, (float) (-magnitudePlotData[cutoff][1] / yscale + y0), cutoffColor);
                canvasMag.drawText("-3", x0*13/20-text.measureText("-3", 0, "-3".length())/2, (float)(-magnitudePlotData[cutoff][1] / yscale + y0 +10), cutoffColor);

                for (int i = 0; i < magnitudePlotData.length; i++) {
                    pointColor.setStrokeWidth(2);
                    if (i > 0) {
                        canvasMag.drawLine((float) magnitudePlotData[i - 1][0] * xLinearScale + x0, (float) (-magnitudePlotData[i - 1][1] / yscale + y0), (float) (magnitudePlotData[i][0] * xLinearScale + x0), (float) (-magnitudePlotData[i][1] / yscale + y0), pointColor);
                        canvasPha.drawLine((float) phasePlotData[i - 1][0] * xLinearScale + x0, (float) (phasePlotData[i - 1][1] / phaseYscale + y0), (float) (phasePlotData[i][0] * xLinearScale + x0), (float) (phasePlotData[i][1] / phaseYscale + y0), pointColor);
                    }
                }
            } else {
                double[] logX = new double[magnitudePlotData.length];
                double stepsize = (fmax-fmin)/magnitudePlotData.length;
                for (int i = 0; i < logX.length; i++) {
                    if (i == 0) {
                        logX[i] = 0;
                    } else {
                        logX[i] = Math.log10(i*stepsize)/Math.log10(fmax-fmin)*plotWidth;
                    }
                }

                canvasMag.drawLine(x0+1, (float) (-magnitudePlotData[cutoff][1] / yscale + y0), xmax-1, (float) (-magnitudePlotData[cutoff][1] / yscale + y0), cutoffColor);
                canvasMag.drawText("-3", x0*13/20-text.measureText("-3", 0, "-3".length())/2, (float)(-magnitudePlotData[cutoff][1] / yscale + y0 +10), cutoffColor);

                for (int i = 1; i < magnitudePlotData.length; i++) {
                    pointColor.setStrokeWidth(3);
                    canvasMag.drawLine((float) (logX[i-1]) + x0, (float) (-magnitudePlotData[i-1][1] / yscale + y0), (float) (logX[i]) + x0, (float) (-magnitudePlotData[i][1] / yscale + y0), pointColor);
                    canvasPha.drawLine((float) (logX[i-1]) + x0, (float) (phasePlotData[i-1][1] / phaseYscale + y0), (float) (logX[i]) + x0, (float) (phasePlotData[i][1] / phaseYscale + y0), pointColor);
                }

            }
        }else{
            if(firstRun == true) {
                dBmin = 0;

                if(Filter_Realtime_Activity.isActive) {
                    if(a != Filter_Realtime_Activity.a || b != Filter_Realtime_Activity.b || order != Filter_Realtime_Activity.order) {
                        firstRun = true;
                        a = Filter_Realtime_Activity.a;
                        b = Filter_Realtime_Activity.b;
                    }
                }else if(Filter_Static_Activity.isActive){
                    if(a != Filter_Static_Activity.a || b != Filter_Static_Activity.b || order != Filter_Static_Activity.order) {
                        firstRun = true;
                        a = Filter_Static_Activity.a;
                        b = Filter_Static_Activity.b;
                    }
                }

                double[] magTemp = new double[complexFFT.length];

                for(int j=0; j < magTemp.length; j++){
                    magTemp[j] = Math.sqrt(Math.pow(complexFFT[j][0],2)+Math.pow(complexFFT[j][1],2)); // Finding the magnitude of the transformed complex signal
                }



                int dotsPrPlot = (int) plotWidth; //512;
                int scale = magTemp.length / dotsPrPlot;    // Plots with a resolution of 256 points pr. PlotFrequencyResponse
                double[] temp1 = new double[(magTemp.length/(scale*2))];  //Same scale algorithm as in frequency-domain-PlotFrequencyResponse
                magnitudePlotData = new double[temp1.length][2];

                double sum = 0;
                double maxVal = 0;

                for (int j = 0; j < temp1.length; j++) {
                    for (int l = 0; l < scale; l++) {
                        sum += magTemp[j * scale + l];
                    }
                    sum /= scale;
                    if (sum > maxVal) {
                        maxVal = sum;
                    }
                    temp1[j] = sum;
                    sum = 0;
                }

                for (int j = 0; j < temp1.length; j++) {
                    // Normalizes the x-axis from 0 to 1
                    // and finds the dB-value of the signal the maxVal as reference value
                    magnitudePlotData[j][0] = (float) j;
                    magnitudePlotData[j][1] = 20 * Math.log10(temp1[j] / maxVal);


                    if (magnitudePlotData[j][1] > dBmax) {
                        dBmax = (float) magnitudePlotData[j][1];
                    }
                    if (j == 0) {
                        dBmin = (float) magnitudePlotData[j][1];
                    }
                    if (magnitudePlotData[j][1] < dBmin) { // If sum are smaller then previous minVal, then new minVal
                        dBmin = (float) magnitudePlotData[j][1];
                    }

                }
                dBspan = dBmax - dBmin;

                firstRun = false;
            }

            drawBodePlotWindow(linLogScale, (int) dBspan, (int) phaSpan, false);

            Paint pointColor = new Paint();
            pointColor.setColor(Color.parseColor("#990000"));
            pointColor.setStyle(Paint.Style.FILL);
            pointColor.setStrokeWidth(3);

            float xLinearScale = plotWidth / magnitudePlotData.length;
            float yscale = dBspan / plotHeight;
            int cutoff = findCutoffPoint(magnitudePlotData);


            if (linLogScale == true) {
                canvasMag.drawLine(x0+1, (float) (-magnitudePlotData[cutoff][1] / yscale + y0), xmax-1, (float) (-magnitudePlotData[cutoff][1] / yscale + y0), cutoffColor);
                canvasMag.drawText("-3", x0*13/20-text.measureText("-3", 0, "-3".length())/2, (float)(-magnitudePlotData[cutoff][1] / yscale + y0 +10), cutoffColor);

                for (int i = 0; i < magnitudePlotData.length; i++) {
                    pointColor.setStrokeWidth(2);
                    if (i > 0) {
                        canvasMag.drawLine((float) magnitudePlotData[i - 1][0] * xLinearScale + x0, (float) (-magnitudePlotData[i - 1][1] / yscale + y0), (float) (magnitudePlotData[i][0] * xLinearScale + x0), (float) (-magnitudePlotData[i][1] / yscale + y0), pointColor);
                    }
                }


            } else {
                double[] logX = new double[magnitudePlotData.length];
                double stepsize = (fmax-fmin)/magnitudePlotData.length;
                for (int i = 0; i < logX.length; i++) {
                    if (i == 0) {
                        logX[i] = 0;
                    } else {
                        logX[i] = Math.log10(i*stepsize)/Math.log10(fmax-fmin)*plotWidth;
                    }
                }

                canvasMag.drawLine(x0+1, (float) (-magnitudePlotData[cutoff][1] / yscale + y0), xmax-1, (float) (-magnitudePlotData[cutoff][1] / yscale + y0), cutoffColor);
                canvasMag.drawText("-3", x0*13/20-text.measureText("-3", 0, "-3".length())/2, (float)(-magnitudePlotData[cutoff][1] / yscale + y0 +10), cutoffColor);

                for (int i = 1; i < magnitudePlotData.length; i++) {
                    pointColor.setStrokeWidth(3);
                    canvasMag.drawLine((float) (logX[i-1]) + x0, (float) (-magnitudePlotData[i-1][1] / yscale + y0), (float) (logX[i]) + x0, (float) (-magnitudePlotData[i][1] / yscale + y0), pointColor);
                }

            }
        }

    }


    private int findCutoffPoint(double[][] data) {
        int target = 3;
        int closest = 0;
        double min = Double.MAX_VALUE;

        for (int i = 0; i < data.length; i++) {
            final double diff = Math.abs(target+data[i][1]);

            if (diff < min) {
                min = diff;
                closest = i;
            }
        }
        return closest;
    }


    public void drawMagnitudeSpectrumData(double[] input, boolean linLogScale, int dynamicRange) {
        if (canvasMag != null) {
            canvasMag.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            dBmin = 0;
            data = input;
            int dotsPrPlot = data.length;
            int scale = data.length / dotsPrPlot;    // Plots with a resolution of 256 points pr. PlotFrequencyResponse
            double[] temp = new double[(data.length / (scale * 2))];  //Same scale algorithm as in frequency-domain-PlotFrequencyResponse
            magnitudePlotData = new double[temp.length][2];

            double sum = 0;
            double maxVal = 0;

            for (int j = 0; j < temp.length; j++) {
                for (int k = 0; k < scale; k++) {
                    sum += data[j * scale + k];
                }
                sum /= scale;
                if (sum > maxVal) {
                    maxVal = sum;
                }
                temp[j] = sum;
                sum = 0;
            }

            for (int j = 0; j < temp.length; j++) {
                // Normalizes the x-axis from 0 to 1
                // and finds the dB-value of the signal the maxVal as reference value
                magnitudePlotData[j][0] = (float) j;
                magnitudePlotData[j][1] = 20 * Math.log10(temp[j] / maxVal);

                if (magnitudePlotData[j][1] > dBmax) {
                    dBmax = (float) magnitudePlotData[j][1];
                }
                if (j == 0) {
                    dBmin = (float) magnitudePlotData[j][1];
                }
                if (magnitudePlotData[j][1] < dBmin) { // If sum are smaller then previous minVal, then new minVal
                    dBmin = (float) magnitudePlotData[j][1];
                }
            }
            dBspan = dBmax - dBmin;

            Paint lineColor = new Paint();
            lineColor.setColor(Color.parseColor("#990000"));
            lineColor.setStyle(Paint.Style.FILL);
            lineColor.setStrokeWidth(2);

            Paint text = new Paint();
            text.setColor(Color.BLACK);
            text.setTextSize(smallText);
            text.setStyle(Paint.Style.FILL);

            float xLinearScale = plotWidth / magnitudePlotData.length;
            float yscale = dynamicRange / plotHeight;

            Paint gridColor = new Paint();
            gridColor.setColor(Color.GRAY);
            gridColor.setStyle(Paint.Style.FILL);
            gridColor.setStrokeWidth(1);

            for (int i = 0; i < dynamicRange; i += 10) {
                canvasMag.drawLine(x0 + 1, i * plotHeight / dynamicRange + y0, xmax - 1, i * plotHeight / dynamicRange + y0, gridColor);
                String number = String.valueOf(-i);
                canvasMag.drawText(number, x0 * 13 / 20 - text.measureText(number, 0, number.length()) / 2, i * plotHeight / dynamicRange + y0 + 10, text);
            }

            if (linLogScale == true) {
                for (int i = 0; i < magnitudePlotData.length; i++) {
                    lineColor.setStrokeWidth(2);
                    if (i > 0) {
                        if (-magnitudePlotData[i - 1][1] / dynamicRange > 1) {
                            magnitudePlotData[i - 1][1] = -dynamicRange;
                        }
                        if (-magnitudePlotData[i][1] / dynamicRange > 1) {
                            magnitudePlotData[i][1] = -dynamicRange;
                        }

                        canvasMag.drawLine((float) magnitudePlotData[i - 1][0] * xLinearScale + x0, (float) (-magnitudePlotData[i - 1][1] / yscale + y0 + 1), (float) (magnitudePlotData[i][0] * xLinearScale + x0), (float) (-magnitudePlotData[i][1] / yscale + y0), lineColor);
                    }
                }
            } else {
                double[] logX = new double[magnitudePlotData.length];
                double stepsize = (fmax - fmin) / magnitudePlotData.length;
                for (int i = 0; i < logX.length; i++) {
                    if (i == 0) {
                        logX[i] = 0;
                    } else {
                        logX[i] = Math.log10(i * stepsize) / Math.log10(fmax - fmin) * plotWidth;
                    }
                }

                for (int i = 1; i < magnitudePlotData.length; i++) {

                    if (-magnitudePlotData[i - 1][1] / dynamicRange > 1) {
                        magnitudePlotData[i - 1][1] = -dynamicRange;
                    }
                    if (-magnitudePlotData[i][1] / dynamicRange > 1) {
                        magnitudePlotData[i][1] = -dynamicRange;
                    }
                    canvasMag.drawLine((float) (logX[i - 1]) + x0, (float) (-magnitudePlotData[i - 1][1] / yscale + y0), (float) (logX[i]) + x0, (float) (-magnitudePlotData[i][1] / yscale + y0), lineColor);
                }
            }
        }
    }

    public void drawMagnitudeSpectrumData(double[] filteredInput, double[] unfilteredInput, boolean linLogScale, int dynamicRange) {
        if (canvasMag != null) {
            canvasMag.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            dBmin = 0;

            data = filteredInput;
            unfilteredData = unfilteredInput;

            int dotsPrPlot = data.length;
            int scale = data.length / dotsPrPlot;    // Plots with a resolution of 256 points pr. PlotFrequencyResponse
            double[] temp1 = new double[(data.length / (scale * 2))];  //Same scale algorithm as in frequency-domain-PlotFrequencyResponse
            magnitudePlotData = new double[temp1.length][2];

            double[] temp2 = new double[(unfilteredData.length / (scale * 2))];  //Same scale algorithm as in frequency-domain-PlotFrequencyResponse
            unfilteredPlotData = new double[temp2.length][2];

            double sum = 0;
            double maxVal = 0;

            for (int j = 0; j < temp1.length; j++) {
                for (int k = 0; k < scale; k++) {
                    sum += data[j * scale + k];
                }
                sum /= scale;
                if (sum > maxVal) {
                    maxVal = sum;
                }
                temp1[j] = sum;
                sum = 0;
            }

            double unfilteredMaxVal = 0;

            for (int j = 0; j < temp2.length; j++) {
                for (int k = 0; k < scale; k++) {
                    sum += unfilteredData[j * scale + k];
                }
                sum /= scale;
                if (sum > unfilteredMaxVal) {
                    unfilteredMaxVal = sum;
                }
                temp2[j] = sum;
                sum = 0;
            }


            for (int j = 0; j < temp1.length; j++) {
                // Normalizes the x-axis from 0 to 1
                // and finds the dB-value of the signal the maxVal as reference value
                magnitudePlotData[j][0] = (float) j;
                magnitudePlotData[j][1] = 20 * Math.log10(temp1[j] / maxVal);

                unfilteredPlotData[j][0] = (float) j;
                unfilteredPlotData[j][1] = 20 * Math.log10(temp2[j] / unfilteredMaxVal);
            }


            Paint pointColor = new Paint();
            pointColor.setColor(Color.parseColor("#306A9F"));
            pointColor.setStyle(Paint.Style.FILL);
            pointColor.setStrokeWidth(2);

            Paint lineColor = new Paint();
            lineColor.setColor(Color.parseColor("#990000"));
            lineColor.setStyle(Paint.Style.FILL);
            lineColor.setStrokeWidth(2);

            Paint text = new Paint();
            text.setColor(Color.BLACK);
            text.setTextSize(smallText);
            text.setStyle(Paint.Style.FILL);

            float xLinearScale = plotWidth / magnitudePlotData.length;
            float yscale = dynamicRange / plotHeight;

            if (linLogScale == true) {
                for (int i = 0; i < magnitudePlotData.length; i++) {
                    if (i > 0) {
                        if (-unfilteredPlotData[i - 1][1] / dynamicRange > 1) {
                            unfilteredPlotData[i - 1][1] = -dynamicRange;
                        }
                        if (-unfilteredPlotData[i][1] / dynamicRange > 1) {
                            unfilteredPlotData[i][1] = -dynamicRange;
                        }

                        canvasMag.drawLine((float) unfilteredPlotData[i - 1][0] * xLinearScale + x0, (float) (-unfilteredPlotData[i - 1][1] / yscale + y0 + 1), (float) (unfilteredPlotData[i][0] * xLinearScale + x0), (float) (-unfilteredPlotData[i][1] / yscale + y0), pointColor);

                        if (-magnitudePlotData[i - 1][1] / dynamicRange > 1) {
                            magnitudePlotData[i - 1][1] = -dynamicRange;
                        }
                        if (-magnitudePlotData[i][1] / dynamicRange > 1) {
                            magnitudePlotData[i][1] = -dynamicRange;
                        }

                        canvasMag.drawLine((float) magnitudePlotData[i - 1][0] * xLinearScale + x0, (float) (-magnitudePlotData[i - 1][1] / yscale + y0 + 1), (float) (magnitudePlotData[i][0] * xLinearScale + x0), (float) (-magnitudePlotData[i][1] / yscale + y0), lineColor);

                    }
                }
            } else {
                double[] logX = new double[magnitudePlotData.length];
                double stepsize = (fmax - fmin) / magnitudePlotData.length;
                for (int i = 0; i < logX.length; i++) {
                    if (i == 0) {
                        logX[i] = 0;
                    } else {
                        logX[i] = Math.log10(i * stepsize) / Math.log10(fmax - fmin) * plotWidth;
                    }
                }

                for (int i = 1; i < magnitudePlotData.length; i++) {
                    if (-unfilteredPlotData[i - 1][1] / dynamicRange > 1) {
                        unfilteredPlotData[i - 1][1] = -dynamicRange;
                    }
                    if (-unfilteredPlotData[i][1] / dynamicRange > 1) {
                        unfilteredPlotData[i][1] = -dynamicRange;
                    }

                    canvasMag.drawLine((float) (logX[i - 1]) + x0, (float) (-unfilteredPlotData[i - 1][1] / yscale + y0), (float) (logX[i]) + x0, (float) (-unfilteredPlotData[i][1] / yscale + y0), pointColor);

                    if (-magnitudePlotData[i - 1][1] / dynamicRange > 1) {
                        magnitudePlotData[i - 1][1] = -dynamicRange;
                    }
                    if (-magnitudePlotData[i][1] / dynamicRange > 1) {
                        magnitudePlotData[i][1] = -dynamicRange;
                    }

                    canvasMag.drawLine((float) (logX[i - 1]) + x0, (float) (-magnitudePlotData[i - 1][1] / yscale + y0), (float) (logX[i]) + x0, (float) (-magnitudePlotData[i][1] / yscale + y0), lineColor);
                }
            }
        }
    }



}






