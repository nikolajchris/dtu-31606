/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikolaj on 25-04-2017.
 */

public class filtertab1_realtime extends Fragment {

    public static Spinner filterSpinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);

        View rootView = inflater.inflate(R.layout.filtertab1_realtime, container, false);

        filterSpinner = (Spinner) rootView.findViewById(R.id.filterSpinner);
        // Creates spinner element(dropdown meu) holding the names of all files in DTU_DSP-folder

        File folder = new File(Environment.getExternalStorageDirectory() + "/DTU_DSP/filters/");
        File[] listOfFiles = folder.listFiles(); //Creates a list of files from folder

        List<String> categories = new ArrayList<String>(); // Spinner Drop down elements
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                categories.add(listOfFiles[i].getName());
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, categories); // Creating adapter for spinner
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Drop down layout style - list view with radio button
            filterSpinner.setAdapter(dataAdapter); // attaching data adapter to spinner
        }




        return rootView;
    }
}
