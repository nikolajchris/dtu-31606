/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.media.MediaRecorder;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


public class RecordActivity extends AppCompatActivity {

    int sampleRate;                 // Sample rate of the recording
    AudioRecord recorder;           // Android audio recorder class
    boolean recording = false;      // unit to determine whether  the system is recording or not
    Spinner spinner;                // Spinner element to hold the available sample rates
    String savefilename;            // Name to save the recording as
    SoundProcessor sound;                // Holds data about recording, used when playing the recording
    byte [] data;                   // Holds recorded sound in bytes before passed to a wave file


    @Override
    protected void onCreate(Bundle savedInstanceState) {    //When activity launches:
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);




        findViewById(R.id.loadingPanelRec).setVisibility(View.INVISIBLE); // Disables loading wheel

        spinner = (Spinner) findViewById(R.id.spinner); // Creating Spinner element for sample rate
        List<String> categories = new ArrayList<String>(); // Spinner Drop down list
        categories.add("8000");
        categories.add("16000");
        categories.add("32000");
        categories.add("44100");
        categories.add("48000");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories); // Creating adapter for spinner
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Drop down layout style - list view with radio button
        spinner.setAdapter(dataAdapter); // attaching data adapter to spinner
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_userguide, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            try{
                Intent i = new Intent(this, RecordInfoActivity.class);
                startActivity(i);
            }catch (Exception e){
                e.printStackTrace();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // This is the on-button-press case-tree for this application window
    public void recordButtonTapped(View view) {

        ImageButton recorder = (ImageButton) findViewById(R.id.Brecorder);
        ImageButton stopRecorder = (ImageButton) findViewById(R.id.BstopRec);

        switch (view.getId()) {
            case R.id.Brecorder:
                sampleRate = Integer.parseInt(spinner.getSelectedItem().toString()); // choose fs
                findViewById(R.id.loadingPanelRec).setVisibility(View.VISIBLE); //Show loading wheel
                try {
                    Thread recordThread = new Thread(new Runnable() {

                        @Override
                        public void run() { // Launches new thread
                            recording = true;  // Set recording to true
                            try {
                                beginRecording();    // Begins recording
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                    });
                    recordThread.start(); // starts thread
                    recorder.setEnabled(false);   //Disables record button
                    stopRecorder.setEnabled(true); //Enable stop-record button
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.BstopRec:
                try {
                    stopRecording(); // Stpos the recording
                    recorder.setEnabled(true); //Enables record button
                    stopRecorder.setEnabled(false); //Disables stop-record button
                    findViewById(R.id.loadingPanelRec).setVisibility(View.INVISIBLE); //Hides loading wheel
                    Toast.makeText(getApplicationContext(), savefilename+".wav has been saved", Toast.LENGTH_LONG).show(); //on-screen message
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Failed to save file", Toast.LENGTH_SHORT).show(); //on-screen message
                }
                break;
            case R.id.Bplay:
                try {
                    sound = loadFile();     // loads the recently created record from file
                    sound.emptyProcess();   // passes an unprocessed signal to the processed array
                    sound.playSound();      // Plays sound
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private int setupRecording() throws IOException { // recorder function
        // finds min. buffersize for the upcomming stream of data
        int minBufferSize = AudioRecord.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

        // creates streaming service from microphone of device
        recorder = new AudioRecord.Builder()
                .setAudioSource(MediaRecorder.AudioSource.DEFAULT)
                .setAudioFormat(new AudioFormat.Builder()
                        .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                        .setSampleRate(sampleRate)
                        .setChannelMask(AudioFormat.CHANNEL_IN_MONO)
                        .build())
                .setBufferSizeInBytes(minBufferSize)
                .build();

        return minBufferSize;
    }

    private void beginRecording() throws IOException { // recorder function
        sampleRate = Integer.parseInt(spinner.getSelectedItem().toString());// finds user defined sample rate
        EditText filenameInput = (EditText) findViewById(R.id.recName);     // Finds user defined filename
        savefilename = filenameInput.getText().toString();                  // passes name to string

        int minBufferSize = setupRecording();
        recorder.startRecording();  // opens the stream

        // Creates byte array to hold the recorded data in bytes
        ByteArrayOutputStream audioStream = new ByteArrayOutputStream();    // Creates byte array to hold the recorded data in bytes

        byte[] audioData = new byte[minBufferSize]; // Creates a  small buffer byte array to hold the recorded data in bytes for each "buff"

        while (recording) {
            int numberOfShort = recorder.read(audioData, 0, minBufferSize); // passes recorder data to the small buffer array
            for (int i = 0; i < numberOfShort; i++) {
                audioStream.write(audioData[i]);
            }
            data = audioStream.toByteArray();   // Writes the buffer-parts dynamicly, expands if needed to hold all the recorded data
        }
        recorder.stop();        // Closes the stream
        recorder.release();


        try { // Calculates paratmeters for a wave header; standard wave file header format
            int bitDepth = 16;  // Chosen bit depth value for this app's recordings
            int channels = 1;   // mono sound
            long subChunk1Size = bitDepth;
            int format = 1;
            int byteRate = sampleRate * channels * bitDepth/8;
            int blockAlign = channels * bitDepth/8;

            long dataSize = data.length;
            long chunk2Size =  dataSize * channels * bitDepth/8;
            long chunkSize = 36 + chunk2Size;

            OutputStream ops = new FileOutputStream(Environment.getExternalStorageDirectory() + "/DTU_DSP/" + savefilename + ".wav");
            BufferedOutputStream bos = new BufferedOutputStream(ops);
            DataOutputStream dos = new DataOutputStream(bos); // starts streaming data to file

            dos.writeBytes("RIFF");                               // 00 - RIFF
            dos.write(intToByteArray((int)chunkSize), 0, 4);//04 - how big is the rest of this file?
            dos.writeBytes("WAVE");                               // 08 - WAVE
            dos.writeBytes("fmt ");                               // 12 - fmt
            dos.write(intToByteArray((int)subChunk1Size), 0, 4);  // 16 - size of this chunk
            dos.write(shortToByteArray((short)format), 0, 2);     // 20 - what is the audio format? 1 for Pulse Code Modulation (PCM)
            dos.write(shortToByteArray((short)channels), 0, 2);   // 22 - mono or stereo? 1 or 2?
            dos.write(intToByteArray(sampleRate), 0, 4);//24-samples per second (numbers per second)
            dos.write(intToByteArray((int)byteRate), 0, 4);         // 28 - bytes per second
            dos.write(shortToByteArray((short)blockAlign), 0, 2); // 32 - number of bytes in one sample, for all channels
            dos.write(shortToByteArray((short)bitDepth), 0, 2);   // 34 - how many bits in a sample(number)?  usually 16 or 24
            dos.writeBytes("data");                               // 36 - data
            dos.write(intToByteArray((int)dataSize), 0, 4);       // 40 - how big is this data chunk
            dos.write(data);        // 44 - the actual data itself - just a long string of numbers
            dos.flush();
            dos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        audioStream.flush();
        audioStream.close(); // closes the stream
    }

    private void stopRecording() throws IOException {  // Stops recording
        recording = false;
    }

    private SoundProcessor loadFile () throws IOException {
        WavImport file = new WavImport();                // Initializes file import procedure
        sound = file.WavImport(savefilename+".wav");    // Get file from folder as saved file name
        return sound;                                   // Returns result of to global SoundProcessor
    }

    private static byte[] shortToByteArray(short data) { // Converts a short into a byte array
        return new byte[]{(byte)(data & 0xFF),(byte)((data >>> 8) & 0xFF)};
    }

    private static byte[] intToByteArray(int i){  // Converts an int to a byte array, up to 4 values
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0xFF);
        b[1] = (byte) ((i >> 8) & 0xFF);
        b[2] = (byte) ((i >> 16) & 0xFF);
        b[3] = (byte) ((i >> 24) & 0xFF);
        return b;
    }

}

