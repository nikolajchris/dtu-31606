/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nikolaj on 25-04-2017.
 */

public class filtertab4 extends Fragment {


    public static ImageView windowArea;
    public static ImageView dataArea;
    public static PlotFrequencyResponse window;
    public static PlotFrequencyResponse data;
    public static Switch linLogSpectrumSwitch;
    public static Switch unfilteredSwith;
    public static Spinner DRspinner;
    public static Spinner RAspinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setRetainInstance(true);

        View rootView = inflater.inflate(R.layout.filtertab4, container, false);

        // Initializes the Dynamic range spinner
        DRspinner = (Spinner) rootView.findViewById(R.id.dynRangeRealtime); // Creating Spinner element

        List<String> DRscale = new ArrayList<String>(); // Spinner Drop down elements
        DRscale.add("10");
        DRscale.add("20");
        DRscale.add("30");
        DRscale.add("40");
        DRscale.add("50");
        DRscale.add("60");
        DRscale.add("70");
        DRscale.add("80");
        DRscale.add("90");
        DRscale.add("100");
        DRscale.add("110");
        DRscale.add("120");

        ArrayAdapter<String> scaleAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, DRscale); // Creating adapter for spinner
        scaleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Drop down layout style - list view with radio button
        DRspinner.setAdapter(scaleAdapter); // attaching data adapter to spinner
        DRspinner.setSelection(5); //Starts at selected position (zero index)

        // Initializes the Dynamic range spinner
        RAspinner = (Spinner) rootView.findViewById(R.id.refreshRateRealtime); // Creating Spinner element

        List<String> RAscale = new ArrayList<String>(); // Spinner Drop down elements
        RAscale.add("Too Fast");
        RAscale.add("Very Fast");
        RAscale.add("Fast");
        RAscale.add("Moderate");
        RAscale.add("Slow");
        RAscale.add("Very Slow");
        RAscale.add("Extremely Slow");

        ArrayAdapter<String> RAscaleAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, RAscale); // Creating adapter for spinner
        RAscaleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // Drop down layout style - list view with radio button
        RAspinner.setAdapter(RAscaleAdapter); // attaching data adapter to spinner
        RAspinner.setSelection(2); //Starts at selected position (zero index)

        linLogSpectrumSwitch = (Switch) rootView.findViewById(R.id.linLogSpectrumSwitch);
        unfilteredSwith = (Switch) rootView.findViewById(R.id.unfilterSwitchRealtime);

        windowArea = (ImageView) rootView.findViewById(R.id.windowPlotArea);
        dataArea = (ImageView) rootView.findViewById(R.id.dataPlotArea);
        window = new PlotFrequencyResponse();  // Initializes plotter
        data  = new PlotFrequencyResponse();  // Initializes plotter

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int w = size.x;
        int h = size.y;

        if(Filter_Realtime_Activity.isActive) {
            if (w < h) {
                window.initMagnitudeSpectrum(windowArea, w, w * 3 / 4, Filter_Realtime_Activity.fmin, Filter_Realtime_Activity.fs/2);
                data.initMagnitudeSpectrum(dataArea, w, w * 3 / 4, Filter_Realtime_Activity.fmin, Filter_Realtime_Activity.fs/2);
            } else {
                window.initMagnitudeSpectrum(windowArea, h, h * 3 / 4, Filter_Realtime_Activity.fmin, Filter_Realtime_Activity.fs/2);
                data.initMagnitudeSpectrum(dataArea, h, h * 3 / 4, Filter_Realtime_Activity.fmin, Filter_Realtime_Activity.fs/2);
            }
        }else if(Filter_Static_Activity.isActive){
            if (w < h) {
                window.initMagnitudeSpectrum(windowArea, w, w * 3 / 4, Filter_Static_Activity.fmin, Filter_Static_Activity.fs/2);
                data.initMagnitudeSpectrum(dataArea, w, w * 3 / 4, Filter_Static_Activity.fmin, Filter_Static_Activity.fs/2);
            } else {
                window.initMagnitudeSpectrum(windowArea, h, h * 3 / 4, Filter_Static_Activity.fmin, Filter_Static_Activity.fs/2);
                data.initMagnitudeSpectrum(dataArea, h, h * 3 / 4, Filter_Static_Activity.fmin, Filter_Static_Activity.fs/2);
            }
        }


        window.drawMagnitudeSpectrumWindow(linLogSpectrumSwitch.isChecked(), 60);

        return rootView;

    }
}
