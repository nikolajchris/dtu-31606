/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.os.Environment;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;


public class WavImport {
    public SoundProcessor WavImport(String filename) throws IOException {
        int infoSize = 256; //size of array, reserved for the wave-file header information.

        String filepath = Environment.getExternalStorageDirectory() + "/DTU_DSP/" + filename; // get storage direction to file
        FileInputStream fin = new FileInputStream(filepath);
        DataInputStream dis = new DataInputStream(fin); // importing data from file through a stream

        byte[] wavInfo = new byte[infoSize]; // Read a small part of the file (header)
        dis.read(wavInfo, 0, wavInfo.length); // Write data from file info wavInfo array
        WaveHeader header = new WaveHeader(wavInfo); // Extract info from wave file header
        System.out.println("File: "+filename);
        header.test(); // print wave file info to the terminal

        byte[] music = new byte[header.waveLength + header.headerLength]; //make array, length of file
        dis.read(music, 0, music.length); // write data into array
        SoundProcessor sound = new SoundProcessor(music, header.headerLength, header.waveLength, header.sampleRate, 16); // create SoundProcessor

        dis.close(); // close data stream
        fin.close(); // close file access

        return sound; // Return the extracted sound data
    }
}
