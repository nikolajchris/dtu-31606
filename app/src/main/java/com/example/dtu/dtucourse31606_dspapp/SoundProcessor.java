/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioTrack;

import com.jjoe64.graphview.GraphView;

public class SoundProcessor {

    public double[] sound;          // Array that contains the unprocessed sound file data
    public double[] soundTransformed; // Array that contains the FFT processed sound file data
    public double[] soundFiltered;  // Array that contains the Filter processed sound file data
    double[][] complexFourier;      // Array that contains the Complex processed sound file data when fourier transformed
    double[][] complexTime;         // Array that contains the Complex processed sound file data when inverse fourier transformed
    public int fs;                  // Sample rate of the sound

    // imports data from the wave header function
    public SoundProcessor(byte[] wave, int dataStart, int dataLength, int sampleRate, int bytesPrSample){
        if(bytesPrSample == 8){
            byte[] sound = new byte[dataLength]; //data is in bytes, sound is in double (= 2*bytes)
            sound = wave;
            fs = sampleRate;
        } else if (bytesPrSample == 16){
            sound = new double[dataLength/2]; //data is in bytes, sound is in double (= 2*bytes)
            fs = sampleRate;
            for(int i=0;i<sound.length;i++){ // Makes one doubles of two bytes, since 16bit pr. sample
                sound[i] = bit16ToDouble(wave[i*2+dataStart],wave[i*2+dataStart+1]);
            }
        }
    }

    public SoundProcessor(double[] data, int sampleRate){
            sound = data;
            fs = sampleRate;
    }

    public SoundProcessor(double[] data){
        sound = data;
    }

    // This function take 16bit and converts them to one double, that can be processed
    private double bit16ToDouble(byte two, byte one){
        int temp;
        temp = (int)one;
        temp = temp << 8;
        temp = (temp & 0xFFFFFF00)|((int)two & 0xFF);
        return (double) temp;
    };


    // If no processing is done, this passes unprocessed data to the processed array
    public void emptyProcess(){
        soundTransformed = new double[sound.length];
        for(int i=0;i<sound.length;i++) {
            soundTransformed[i] = sound[i];
        }
    }

    // This is the standard Discrete Fourier Transform(DFT) algorithm
    private double[][] partDft(double[] wave){
        double[][] dftPart = new double[wave.length][2];
        double re, im, angle;

        //DFT: X(k) = sum(0->(N-1)) x(n)*e^(-2*pi*i*k*n/N)
        for(int k=0; k < dftPart.length; k++){
            re=0;   // real part of the dft
            im=0;   // imaginary part of the dft

            for(int n=0; n < dftPart.length; n++){
                angle = -2*Math.PI*k*n/dftPart.length; // Angle only calc'ed once to save operations
                re+=wave[n]*Math.cos(angle);           // Calc real part using Euler's identity
                im-=wave[n]*Math.sin(angle);           // Calc imaginary part using Euler's identity
            }
            dftPart[k][0] = re;     // contains the resulting real part of the dft
            dftPart[k][1] = im;     // contains the resulting imaginary part of the dft
        }

        return dftPart;     // returns result of the dft
    }

    // This dft can be used instead of the fft, but is much slower, since it is working linearly
    public void dft(){
        soundTransformed = new double[sound.length];
        complexFourier = partDft(sound);
        for(int r=0; r < sound.length; r++){
            soundTransformed[r] = Math.sqrt(Math.pow(complexFourier[r][0],2)+Math.pow(complexFourier[r][1],2));
        }

    }

    // This function splits samples of a data array into even and odds. Used during fft
    private double[] splitSound(double[] wave, int odd){
        double[] temp = new double[wave.length/2];
        for (int i=0; i < temp.length; i++){
            temp[i]=wave[i*2+odd];
        }
        return temp;
    }

    // This function zeropads a signal to given padLength
    public double[] zeroPad(double[] wave, int padLength){
        double[] paddedWave = new double[padLength];
        for(int i = 0; i < wave.length; i++){
            paddedWave[i]=wave[i];
        }
        for(int i = wave.length; i < padLength; i++){
            paddedWave[i]=0;
        }
        return paddedWave;
    }

    // This function implements the cooley-tukey fast fourier transform algorithm.
    // This implementation is a recursive function that will divide the input signal into several
    // pairs of even and odd signals until there is only on even and one odd value to process.
    // Then all the pairs get processed parallel, minimizing the dft size, speeding up the process.
    public double[][] partFft(double[] wave){
        if(wave.length > 2) { // Runs this split up routine if the signal is bigger than 2 samples
            double[][] dft = new double[wave.length][2]; // prepare an array to contain the data

            // The function splits itself into an even and an odd part over and over until a
            // "even and odd"-pair only contains two samples.
            // Hereby doing parallel computations instead of linear to speed up the transformation.
            double[][] evenDft = partFft(splitSound(wave, 0));  // splitting sound, returns even
            double[][] oddDft = partFft(splitSound(wave, 1));   // splitting sound, returns odd

            // calculating once: Twiddle factor inner function: e^(-2*pi*i*k/N)
            double twiddle = 2.0 * Math.PI / wave.length;

            // Using the cooley-tukey algorithm:
            // X(k)=Even(k)+e^(twiddle)*Odd(k) and X(k+N/2)=Even(k+N/2)-e^(twiddle)*Odd(k+N/2)
            // Rewritten using Euler's identity
            for (int i = 0; i < wave.length / 2; i++) {
                dft[i][0] = evenDft[i][0] + Math.cos(twiddle * i) * oddDft[i][0] + Math.sin(twiddle * i) * oddDft[i][1]; // Real part of first half of the signal
                dft[i][1] = evenDft[i][1] + Math.cos(twiddle * i) * oddDft[i][1] - Math.sin(twiddle * i) * oddDft[i][0]; // Imaginary part of first half of the signal
                dft[i + wave.length / 2][0] = evenDft[i][0] - Math.cos(twiddle * i) * oddDft[i][0] - Math.sin(twiddle * i) * oddDft[i][1]; // Real part of second half of the signal
                dft[i + wave.length / 2][1] = evenDft[i][1] - Math.cos(twiddle * i) * oddDft[i][1] + Math.sin(twiddle * i) * oddDft[i][0]; // Imaginary part of second half of the signal
            }
            return dft; // returns the result to be split once more
        }else{ // When the signal is split into pairs of evens and odds of only two samples pr. pair
            return partDft(wave);   // Do fourier transform on only one "even and odd"-pair
        }
    }

    // This is the FFT function that is called by the activity-files. FFT of an unprocessed signal
    public void fft(){
        int i = 2; // initializes i
        while (i < sound.length){ //finding nearest power of 2 to the signal. Used for zero padding
            i*=2;
        }
        double[] paddedSound = zeroPad(sound, i);   //zero-padding to the nearest power of 2
        complexFourier = partFft(paddedSound);  // Do recursive FFT-algorithm on the padded signal. This array is kept, in case of an IFFT.
        soundTransformed = new double[paddedSound.length];    // Initialize processed signal array
        for(int j=0; j < paddedSound.length; j++){ // Finding the power of the transformed signal
            soundTransformed[j] = Math.sqrt(Math.pow(complexFourier[j][0],2)+Math.pow(complexFourier[j][1],2));
        }
    }

    // This is the FFT function that is called by the activity-files. FFT of an unprocessed signal
    public void fftOnFilteredSound(){
        int i = 2; // initializes i
        while (i < soundFiltered.length){ //finding nearest power of 2 to the signal. Used for zero padding
            i*=2;
        }
        double[] paddedSound = zeroPad(soundFiltered, i);   //zero-padding to the nearest power of 2
        complexFourier = partFft(paddedSound);  // Do recursive FFT-algorithm on the padded signal. This array is kept, in case of an IFFT.
        soundTransformed = new double[paddedSound.length];    // Initialize processed signal array
        for(int j=0; j < paddedSound.length; j++){ // Finding the power of the transformed signal
            soundTransformed[j] = Math.sqrt(Math.pow(complexFourier[j][0],2)+Math.pow(complexFourier[j][1],2));
        }
    }

    //FFT of processed signal e.i. a filtered signal(same as fft, except using soundTransformed).
    public void processedFft(){ //fft of pre-processed signal(same as fft, except using soundTransformed)
        int i = 2;
        while (i<soundTransformed.length){
            i*=2;
        }
        double[] paddedSound = zeroPad(soundTransformed, i);
        complexFourier = partFft(paddedSound);
        soundTransformed = new double[paddedSound.length];
        for(int j=0; j < paddedSound.length; j++){
            soundTransformed[j] = Math.sqrt(Math.pow(complexFourier[j][0],2)+Math.pow(complexFourier[j][1],2));
        }
    }


    // This is the standard Inverse Discrete Fourier Transform(IDFT) algorithm
    public double[][] partIDFT(double[][] data){
        double[][] idftPart = new double[data.length][2];
        double re, im, angle;

        for(int k=0; k < data.length; k++){
            re=0;
            im=0;

            //IDFT: x(k) = 1/N * sum(0->(N-1)) X(n)*e^(2*pi*i*k*n/N) using Euler's identity
            for(int n=0; n < data.length; n++){
                angle = 2.0*Math.PI*k*n/data.length;                        // 2*pi*i*k*n/N
                re+=data[n][0]*Math.cos(angle)-data[n][1]*Math.sin(angle);  // real part
                im-=data[n][1]*Math.cos(angle)-data[n][0]*Math.sin(angle);  // Imaginary part
            }
            idftPart[k][0] = re;
            idftPart[k][1] = im;
        }
        return idftPart; // The division with signal length is done when function is called
    }

/*    // This idft can be used instead of the ifft, but is much slower, since it is working linearly
    public void idft(){
        soundTransformed = new double[sound.length];
        complexTime = partIDFT(complexFourier);
        for(int r=0; r < sound.length; r++){
            soundTransformed[r] = complexTime[r][0]/complexTime.length;
        }
    }
*/

    // Splits a fourier transformed signal in evens and odds
    private double[][] splitFourier(double[][] data, int odd){
        double[][] temp = new double[data.length/2][2];
        for (int i=0; i < temp.length; i++){
            temp[i][0]=data[i*2+odd][0];
            temp[i][1]=data[i*2+odd][1];
        }
        return temp;
    }

    // This function implements the cooley-tukey algorithm, used for Inverse fast fourier transform
    // The same procedure as partFFT (look above), except using partIFFT as recursive function and
    // taking a complex (double array) argument instead of a real(single array), as in the partFFT.
    private double[][] partIFFT(double[][] data){
        if(data.length > 2) {

            double[][] IDFT = new double[data.length][2];

            double[][] evenIDFT = partIFFT(splitFourier(data, 0));
            double[][] oddIDFT = partIFFT(splitFourier(data, 1));

            double twiddle = 2.0 * Math.PI / data.length;

            for (int i = 0; i < data.length / 2; i++) {
                IDFT[i][0] = evenIDFT[i][0] + Math.cos(twiddle * i) * oddIDFT[i][0] + Math.sin(twiddle * i) * oddIDFT[i][1];
                IDFT[i][1] = evenIDFT[i][1] + Math.cos(twiddle * i) * oddIDFT[i][1] - Math.sin(twiddle * i) * oddIDFT[i][0];
                IDFT[i + data.length / 2][0] = evenIDFT[i][0] - Math.cos(twiddle * i) * oddIDFT[i][0] - Math.sin(twiddle * i) * oddIDFT[i][1];
                IDFT[i + data.length / 2][1] = evenIDFT[i][1] - Math.cos(twiddle * i) * oddIDFT[i][1] + Math.sin(twiddle * i) * oddIDFT[i][0];
            }
            return IDFT;
        }else{
            return partIDFT(data);
        }
    }

    // This is the IFFT function that is called by the activity-files.
    public void ifft(){
        complexTime = partIFFT(complexFourier);         // Calling the recursive partIFFT
        soundTransformed = new double[complexTime.length];// Initializing array for processed data
        for(int i=0; i < complexTime.length; i++){ // writing processed data to array
            soundTransformed[i] = complexTime[i][0]/complexTime.length;  // real part is division with signal length to get the correct amplitude (1/N)
        }
    }

    // Function to zero-pad signals before convolution in frequency domain
    // Zeropads both signals to their combined total length - 1
    public void zeroTofit(SoundProcessor IR){
        double[] temp1 = new double[sound.length+IR.sound.length-1];
        double[] temp2 = new double[sound.length+IR.sound.length-1];

        // Zero padding the signal
        for(int i = 0; i < sound.length; i++){
            temp1[i] = sound[i];
        }
        for(int i = sound.length; i < temp1.length; i++){
            temp1[i] = 0;
        }
        sound = new double[temp1.length];
        for(int i = 0; i < temp1.length; i++){
            sound[i] = temp1[i];
        }

        // zero padding the impulse response
        for(int i = 0; i < IR.sound.length; i++){
            temp2[i] = IR.sound[i];
        }
        for(int i = IR.sound.length; i < temp2.length; i++){
            temp2[i] = 0;
        }
        IR.sound = new double[temp2.length];
        for(int i = 0; i < temp2.length; i++){
            IR.sound[i] = temp2[i];
        }
    }

    // Circular Convolution (in Frequency domain) - uses complex multiplication
    public void conv(double[][] IR){
        double[] temp = new double[complexFourier.length]; // Creates a temporary array

        // Multiplies the complex FFT'ed signal with the complex FFT'ed impulse response
        // (a+bi)(c+di) = (ac-db) + (ad+bc)i
        for (int i = 0; i < complexFourier.length; i++){
            temp[i] = (this.complexFourier[i][0] * IR[i][0] - complexFourier[i][1] * IR[i][1])/complexFourier.length;   // Division to keep the right amplitude when ifft
            complexFourier[i][1] = (complexFourier[i][0] * IR[i][1] + complexFourier[i][1] * IR[i][0])/complexFourier.length;
            complexFourier[i][0] = temp[i];
        }
    }

    public void normalize(){    // Normalizes sound to max value of a 16bit number (32767)
        double maxVal = 0;
        for (int i=0; i < soundFiltered.length; i++) {
            if (soundFiltered[i] > maxVal) {   // if new value is greater than previous peak value
                maxVal = soundFiltered[i];     // New value is the peak value
            }
        }
        double normVal = (32767/maxVal);  // sound*x = double maximum -> max/soundMax = x
        for (int i=0; i < soundFiltered.length; i++) {
            soundFiltered[i] = soundFiltered[i]*normVal;
        }
    }

    // Function used to play a processed SoundProcessor sound
    public void playSound(){
        Runnable play = new Runnable() {
            @Override
            public void run() {  // Launches a separate thread for this task
                try {
                    // Converts double data array to bytes, since Audiotrack writes data in bytes
                    byte[] bytes = doubleToByteArray(sound);

                    // Calculates a min. buffersize for the buffer used in the upcomming data stream
                    int minBufferSize = android.media.AudioTrack.getMinBufferSize(fs, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);

                    // Streams data at a rate calculated from the given parameters
                    AudioTrack at = new AudioTrack.Builder()
                            .setAudioAttributes(new AudioAttributes.Builder()
                                    .setUsage(AudioAttributes.USAGE_MEDIA)
                                    .setContentType(AudioAttributes.CONTENT_TYPE_UNKNOWN)
                                    .build())
                            .setAudioFormat(new AudioFormat.Builder()
                                    .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                                    .setSampleRate(fs)
                                    .setChannelMask(AudioFormat.CHANNEL_OUT_DEFAULT)
                                    .build())
                            .setBufferSizeInBytes(minBufferSize)
                            .build();
                    at.play();  // Opens the stream to the speakers
                    at.write(bytes, 0, bytes.length); // Writes data to the stream, hence playing
                    at.stop(); // Stops the stream when there are no more data
                    at.release();
                } catch (Exception e) {
                }
            }
        };
        Thread playThread = new Thread(play);
        playThread.start(); // start thread
    }



    // Function used to play a processed SoundProcessor sound
    public void playProcessedSound(){
        Runnable play = new Runnable() {
            @Override
            public void run() {  // Launches a separate thread for this task
                try {
                    // Converts double data array to bytes, since Audiotrack writes data in bytes
                    byte[] bytes = doubleToByteArray(soundTransformed);

                    // Calculates a min. buffersize for the buffer used in the upcomming data stream
                    int minBufferSize = android.media.AudioTrack.getMinBufferSize(fs, AudioFormat.CHANNEL_CONFIGURATION_MONO, AudioFormat.ENCODING_PCM_16BIT);

                    // Streams data at a rate calculated from the given parameters
                    AudioTrack at = new AudioTrack.Builder()
                            .setAudioAttributes(new AudioAttributes.Builder()
                                    .setUsage(AudioAttributes.USAGE_MEDIA)
                                    .setContentType(AudioAttributes.CONTENT_TYPE_UNKNOWN)
                                    .build())
                            .setAudioFormat(new AudioFormat.Builder()
                                    .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                                    .setSampleRate(fs)
                                    .setChannelMask(AudioFormat.CHANNEL_OUT_DEFAULT)
                                    .build())
                            .setBufferSizeInBytes(minBufferSize)
                            .build();
                    at.play();  // Opens the stream to the speakers
                    at.write(bytes, 0, bytes.length); // Writes data to the stream, hence playing
                    at.stop(); // Stops the stream when there are no more data
                    at.release();
                } catch (Exception e) {
                }
            }
        };
        Thread playThread = new Thread(play);
        if (!playThread.isAlive()) {
            playThread.start(); // start thread
        }
    }


    // Function that converts a double-array to a byte-array
    private byte[] doubleToByteArray(double[] data) {
        byte[] b = new byte[data.length*2];            // Needed since doubles have a 16bit depth
        for (int i = 0; i < data.length; i++) {        // while bytes have a 8bit depth
            int temp = (int) data[i];
            b[i*2] = (byte) (temp & 0xFF);
            b[i*2+1] = (byte) ((temp >> 8) & 0xFF);
        }
        return b;
    }

    public void IIRfilter(double[] a, double[] b, int order){
        double[] x = sound;
        final double[] y = new double[x.length];

        // making sure a0 is always 1
        double[] temp = new double[a.length+1];
        temp[0] = 1;
        for (int i = 0; i < a.length; i++){
            temp[i+1] = a[i];
        }
        a = new double[temp.length];
        for (int i = 0; i < temp.length; i++){
            a[i] = temp[i];
        }

        //REGULAR IIR/FIR FILTER
        if(order+1 <= b.length || order+1 <= a.length) {

            //When the filter length is greater than the signal this introduction is rolled
            int offset = 0; // offset to keep track of how many samples that are to be processed
            for (int n = 0; n < b.length; n++) { //b-coefficients influence
                for (int k = 0; k <= offset; k++) {
                    y[n] += b[k] * x[n - k + offset];
                }
                offset++;
            }
            offset = 0;
            for (int n = 0; n < a.length; n++) {
                for (int k = 1; k <= offset; k++) { //a-coefficients influence
                    y[n] -= a[k] * y[n - k + offset];
                }
                offset++;
            }

            //When the filter length is equal or less than the signal normal filtering can proceed
            for (int n = b.length; n < x.length; n++) { //b-coefficients influence
                for (int k = 0; k < b.length; k++) {
                    y[n] += b[k] * x[n - k];
                }
            }
            for (int n = a.length; n < x.length; n++) { //a-coefficients influence
                for (int k = 1; k < a.length; k++) {
                    y[n] -= a[k] * y[n - k];
                }
            }
        }
        soundFiltered = y;
    }

    public void continuesIIRfilter(double[] a, double[] b, int order, boolean continued, double[] xleft, double[] yleft){
        double[] x = sound;
        final double[] y = new double[x.length];

        // making sure a0 is always 1
        double[] temp = new double[a.length+1];
        temp[0] = 1;
        for (int i = 0; i < a.length; i++){
            temp[i+1] = a[i];
        }
        a = new double[temp.length];
        for (int i = 0; i < temp.length; i++){
            a[i] = temp[i];
        }

        if (continued = false){
            //REGULAR IIR/FIR FILTER
            if(order+1 <= b.length || order+1 <= a.length) {

                //When the filter length is greater than the signal this introduction is rolled
                int offset = 0; // offset to keep track of how many samples that are to be processed
                for (int n = 0; n < b.length; n++) { //b-coefficients influence
                    for (int k = 0; k <= offset; k++) {
                        y[n] += b[k] * x[n - k + offset];
                    }
                    offset++;
                }
                offset = 0;
                for (int n = 0; n < a.length; n++) {
                    for (int k = 1; k <= offset; k++) { //a-coefficients influence
                        y[n] -= a[k] * y[n - k + offset];
                    }
                    offset++;
                }

                //When the filter length is equal or less than the signal normal filtering can proceed
                for (int n = b.length; n < x.length; n++) { //b-coefficients influence
                    for (int k = 0; k < b.length; k++) {
                        y[n] += b[k] * x[n - k];
                    }
                }
                for (int n = a.length; n < x.length; n++) { //a-coefficients influence
                    for (int k = 1; k < a.length; k++) {
                        y[n] -= a[k] * y[n - k];
                    }
                }
            }
            continued = true;
        } else {
            //REGULAR IIR/FIR FILTER
            if(order+1 <= b.length || order+1 <= a.length) {
                //When the filter length is equal or less than the signal normal filtering can proceed
                for (int n = 0; n < x.length; n++) { //b-coefficients influence
                    for (int k = 0; k < b.length; k++) {
                        if(n < b.length-1){
                            y[n] += b[k] * xleft[n];
                        }else {
                            y[n] += b[k] * x[n - k];
                        }
                    }
                }
                for (int n = 0; n < x.length; n++) { //a-coefficients influence
                    for (int k = 1; k < a.length; k++) {
                        if(n < a.length-1){
                            y[n] -= a[k] * yleft[n];
                        }else {
                            y[n] -= a[k] * y[n - k];
                        }
                    }
                }
            }
        }
        soundFiltered = y;
    }

}