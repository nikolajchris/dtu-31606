/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {    //When activity launches:
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);


        File myFolder = new File(Environment.getExternalStorageDirectory()+"/DTU_DSP");
        if(!myFolder.exists()){
            myFolder.mkdirs();
            try {
                copyWaveFiles(getResources().openRawResource(R.raw.guitar), "guitar");
                copyWaveFiles(getResources().openRawResource(R.raw.sax), "sax");
                copyWaveFiles(getResources().openRawResource(R.raw.sinus), "sinus");
                copyWaveFiles(getResources().openRawResource(R.raw.sweep), "sweep");
                copyWaveFiles(getResources().openRawResource(R.raw.energy), "Energy");
                copyWaveFiles(getResources().openRawResource(R.raw.meant_to_be), "Meant to be");
                copyWaveFiles(getResources().openRawResource(R.raw.jazzy_frenchy), "Jazzy Frenchy");
                copyWaveFiles(getResources().openRawResource(R.raw.happy_rock), "Happy Rock");
                copyWaveFiles(getResources().openRawResource(R.raw.acoustic_breeze), "Acoustic Breeze");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File filterFolder = new File(Environment.getExternalStorageDirectory()+"/DTU_DSP/filters");
        if(!filterFolder.exists()) {
            filterFolder.mkdirs();
            try {
                copyTextFiles(getResources().openRawResource(R.raw.butterworth_2nd_05), "2nd butterworth 0.5");
                copyTextFiles(getResources().openRawResource(R.raw.butterworth_3rd_05), "3rd butterworth 0.5");
                copyTextFiles(getResources().openRawResource(R.raw.all_pass_filter), "All-pass filter");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void copyWaveFiles(InputStream in, String filename) throws IOException {
        OutputStream ops =  new FileOutputStream(Environment.getExternalStorageDirectory()+"/DTU_DSP/"+filename+".wav");
        BufferedOutputStream bos = new BufferedOutputStream(ops);
        DataOutputStream dos = new DataOutputStream(bos); // starts streaming data to file

        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            dos.write(buffer, 0, read);
        }
        in.close();
        dos.flush();
        dos.close();
    }

    private void copyTextFiles(InputStream in, String filename) throws IOException {
        OutputStream ops =  new FileOutputStream(Environment.getExternalStorageDirectory()+"/DTU_DSP/filters/"+filename);
        BufferedOutputStream bos = new BufferedOutputStream(ops);
        DataOutputStream dos = new DataOutputStream(bos); // starts streaming data to file

        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            dos.write(buffer, 0, read);
        }
        in.close();
        dos.flush();
        dos.close();
    }

    // This is the on-button-press case-tree for this application window
    public void startButtonTapped(View view){
        switch(view.getId()){
            case R.id.Bfilter:  // launching filter activity
                try{
                    Intent i = new Intent(this, Filter_Static_Activity.class);
                    startActivity(i);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.Blive:    // launching time domain activity
                try{
                    Intent i = new Intent(this, Filter_Realtime_Activity.class);
                    startActivity(i);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.Brecord:  // launching recorder activity
                try{
                    Intent i = new Intent(this, RecordActivity.class);
                    startActivity(i);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            /*
            case R.id.Bfreq:    // launching frequency domain activity
                try{
                    Intent i = new Intent(this, FreqActivity.class);
                    startActivity(i);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
                */
            case R.id.Binfo:  // launching application information activity
                try{
                    Intent i = new Intent(this, StartInfoActivity.class);
                    startActivity(i);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case R.id.BWaveInfo:    // launching wave manager activity
                try{
                    Intent i = new Intent(this, WaveCheckActivity.class);
                    startActivity(i);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }
}



