/**
 * Created by:
 * Nikolaj Christiansen
 * Bachelor in Electrical Engineering and Master Student
 * Email: s134108@student.dtu.dk
 * Technical University of Denmark
 * July 2017
 */

package com.example.dtu.dtucourse31606_dspapp;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


public class WaveGenerator {

    public int dataLength;
    public int sampleRate;
    public int channels;
    public int bitDepth;
    public double[] rawData;
    public byte[] signal;
    public String filename;
    public File file;

    // Imports data to make file
    public WaveGenerator(String name, int dataLen, int fs, int chan, int bitDep, double[] data, File inputFile){
        dataLength = dataLen;
        sampleRate = fs;
        channels = chan;
        bitDepth = bitDep;
        rawData = data;
        signal = doubleToByteArray(data);//converts 16bit(pr sample) double array to 8bit byte array
        filename = name;
        file = inputFile;
    }

    public void writeWAV(){  // converts given information into a standard wave file
        try {
            long subChunk1Size = bitDepth;
            int format = 1;
            int byteRate = sampleRate * channels * bitDepth/8;
            int blockAlign = channels * bitDepth/8;

            long dataSize = signal.length;
            long chunk2Size =  dataSize * channels * bitDepth/8;
            long chunkSize = 36 + chunk2Size;

            OutputStream ops = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(ops);
            DataOutputStream dos = new DataOutputStream(bos);

            System.out.println("Creating file: "+filename);

            dos.writeBytes("RIFF");                               // 00 - RIFF
            dos.write(intToByteArray((int)chunkSize), 0, 4);      // 04 - how big is the rest of this file?
            dos.writeBytes("WAVE");                               // 08 - WAVE
            dos.writeBytes("fmt ");                               // 12 - fmt
            dos.write(intToByteArray((int)subChunk1Size), 0, 4);  // 16 - size of this chunk
            dos.write(shortToByteArray((short)format), 0, 2);     // 20 - what is the audio format? 1 for Pulse Code Modulation (PCM)
            dos.write(shortToByteArray((short)channels), 0, 2);   // 22 - mono or stereo? 1 or 2?
            dos.write(intToByteArray(sampleRate), 0, 4);          // 24 - samples per second (numbers per second)
            dos.write(intToByteArray((int)byteRate), 0, 4);       // 28 - bytes per second
            dos.write(shortToByteArray((short)blockAlign), 0, 2); // 32 - number of bytes in one sample, for all channels
            dos.write(shortToByteArray((short)bitDepth), 0, 2);   // 34 - how many bits in a sample(number)?  usually 16 or 24
            dos.writeBytes("data");                               // 36 - data
            dos.write(intToByteArray((int)dataSize), 0, 4);       // 40 - how big is this data chunk
            dos.write(signal);                                    // 44 - the actual data itself - just a long string of numbers
            dos.flush();
            dos.close();

            System.out.println("Finished file: "+filename);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // converts an int to a byte array
    private static byte[] intToByteArray(int i){
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0xFF);
        b[1] = (byte) ((i >> 8) & 0xFF);
        b[2] = (byte) ((i >> 16) & 0xFF);
        b[3] = (byte) ((i >> 24) & 0xFF);
        return b;
    }

    // converts a short to a byte array
    private static byte[] shortToByteArray(short data) {
        return new byte[]{(byte)(data & 0xFF),(byte)((data >>> 8) & 0xFF)};
    }

    // converts a double to a byte array
    private byte[] doubleToByteArray(double[] data) {
        byte[] b = new byte[data.length*2];
        for (int i = 0; i < data.length; i++) {
            int temp = (int) data[i];
            b[i*2] = (byte) (temp & 0xFF);
            b[i*2+1] = (byte) ((temp >> 8) & 0xFF);
        }
        return b;
    }
}
